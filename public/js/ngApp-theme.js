/**
 * Created by emaxe on 10.07.2015.
 */

var app = angular.module('app', ['ngResource', 'toastr', 'ngMask', 'ngFileUpload']);

app.config(function(toastrConfig) {
    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,
        newestOnTop: true,
        positionClass: 'toast-bottom-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });
});

app.factory('app', function(){
    console.log('app is init!');
    var that = this;

    return this;
});




/**
 * Created by emaxe on 13.07.2015.
 */

app.factory('Config',function(){
    var that = this;

    this.meta = {};

    this.meta.token = $('meta[name=_token]').attr('content');
    this.meta.host = $('meta[name=_app_host]').attr('content');

    this.mainUrl = 'http://admin.'+this.meta.host;
    this.publicUrl = 'http://'+this.meta.host;

    return this;
});
/**
 * Created by emaxe on 10.07.2015.
 */

app.controller('mainCtrl', function($window, $timeout, $scope, app, toastr, rContent, rGallery, rSms, Upload){
    console.log('mainCtrl is ready!');
    var that = this;
    $scope.app = app;

    fundament_id = 0;
    showOverlay = false;
    this.windowSize = {
        w:0,
        h:0
    };



    this.codeGen = function()
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for( var i=0; i < 5; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    };


    this.init = function(){

    };

    this.os = {
        is_ShowModal: false,
        name: "",
        phone: "",
        email: '',
        contacts: "",
        text: "",
        errorMsg: "",
        fSms: false,
        fToast: false,
        showModal: function(){
            that.os.clear();
            that.os.is_ShowModal = true;
        },
        hideModal: function(){
            that.os.is_ShowModal = false;
        },
        clear: function(){
            if(!that.os.fSms && !that.os.fToast){
                that.os.name = '';
                that.os.phone = '';
                that.os.email = '';
                that.os.contacts = '';
                that.os.text = '';
                that.os.hideModal();
            }
        },
        send: function(ev, file){
            ev.preventDefault();
            ev.stopPropagation();
        // send: function(file){

            if(file==undefined) file = null;

            toastr.clear();
            toastr.info('', 'Заявка обрабатывается..');

            if(that.os.phone!='' || that.os.email!=''){
                that.os.fSms = true;
                that.os.fToast = true;

                rSms.send(that.os, function(data){
                    console.info('rSms.send - ', data) ;
                    that.os.fSms = false;
                    that.os.clear();
                });

                rContent.addToast({item: that.os}, function(data){
                    console.info('rContent.addToast - form', data);

                    that.os.fToast = false;

                    toastr.clear();
                    toastr.info('Мы свяжемся с вами в ближайшее время!', 'Заявка принята');

                    that.os.clear();



                    var id = data.item.id;

                    if(file!=undefined && file!=null){
                        file.upload = Upload.upload({
                            //url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                            url: 'http://zabor-rostov.ru/publicFileUpload/upload',
                            data: {file: file, id: id},
                        });

                        file.upload.then(function (response) {
                            $timeout(function () {
                                file.result = response.data;
                            });
                        }, function (response) {
                            if (response.status > 0)
                                that.os.errorMsg = response.status + ': ' + response.data;
                        }, function (evt) {
                            // Math.min is to fix IE which reports 200% sometimes
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }

                });
                console.info(that.os);
                // that.hidePopup();
            }else{
                toastr.clear();
                toastr.error('Ошибка!', 'Телефон либо email должны быть указаны!');
            }
        }
    };

    this.order = {
        name: '',
        phone: '',
        text: '',
        onClick: function(ev){
            ev.preventDefault();
            toastr.clear();
            toastr.info('', 'Заявка обрабатывается..');

            if(that.order.phone!=''){

                rSms.send(that.order, function(data){
                    console.info('rSms.send - ', data) ;
                });

                rContent.addToast({item: that.order}, function(data){
                    console.info('rContent.addToast - form', data);

                    toastr.clear();
                    toastr.info('Мы свяжемся с вами в ближайшее время!', 'Заявка принята');
                    that.order.name = '';
                    that.order.phone = '';
                    that.order.text = '';

                });
                console.info(that.order);
                that.hidePopup();
            }else{
                toastr.clear();
                toastr.error('Ошибка!', 'Телефон должен быть указан!');
            }


        }
    };

    this.selectMaterial = function(ev, e){
        var block = document.getElementById('block-promo1');
        var items = block.getElementsByClassName('item');
        var itemContents = block.getElementsByClassName('item-content');

        // console.info(items, itemContents);

        for(var i=0;i<items.length;i++){
            items[i].classList.remove('active');
        }

        for(var i=0;i<itemContents.length;i++){
            itemContents[i].classList.remove('active');
        }

        // console.info(ev.target);
        var item = ev.target;
        var i = item.getAttribute('i');
        console.info(item, item.getAttribute('i'), i);
        var itemContent = document.getElementById('itemContent'+item.getAttribute('i'));
        item.classList.add('active')
        itemContent.classList.add('active')
    };

    that.init();

    console.log('mainCtrl is end!');

});
/**
 * Created by emaxe on 22.09.2015.
 */

app.directive('ngPartners', function(){
    return {
        restrict: 'EA',
        scope: {},
        link: function(scope, e, attrs){
            console.info('ngPartners => ', e[0]);
            scope.line = e[0].getElementsByClassName('items-line')[0];
            scope.btns = e[0].getElementsByClassName('btns')[0];
            scope.btnLeft = scope.btns.getElementsByClassName('btn-left')[0];
            scope.btnRight = scope.btns.getElementsByClassName('btn-right')[0];

            var init = function(){
                scope.items = scope.line.getElementsByClassName('item');
                console.info('ngPartners - init => ', scope.els);

                if(scope.items.length < 6){
                    scope.btns.style.display = 'none';
                }

                var w = 0;
                var f =true;

                for(var i=0;i<scope.items.length;i++){
                    // scope.items[i].style.position = 'absolute';
                    // scope.items[i].style.left = w+'px';
                    w += scope.items[i].offsetWidth+18;

                    if(scope.items[i].offsetWidth<20){
                        f = false;
                        setTimeout(function(){
                            init();
                        }, 200);
                        break;
                    }
                }

                scope.line.style.width = w+'px';
                scope.w = w;

            };

            var setEase = function(fEase){
                if(fEase){
                    scope.line.classList.add('ease');
                }else{
                    scope.line.classList.remove('ease');
                }
            };

            scope.goLeft = function(){
                console.info('ngPartners - goLeft => ');
                setEase(true);
                scope.line.style.left = (scope.line.offsetLeft-scope.items[0].offsetWidth)+'px';
                setTimeout(function(){
                    setEase(false);
                    scope.line.style.left = '0px';
                    var e_item = scope.items[0];
                    scope.line.removeChild(e_item);
                    scope.line.insertBefore(e_item, null);
                    scope.items = scope.line.getElementsByClassName('item');
                },400);
            };

            scope.goRight = function(){
                console.info('ngPartners - goRight => ');
                setEase(true);
                scope.line.style.left = (scope.line.offsetLeft+scope.items[0].offsetWidth)+'px';
                setTimeout(function(){
                    setEase(false);
                    scope.line.style.left = '0px';
                    var e_item = scope.items[scope.items.length-1];
                    scope.line.removeChild(e_item);
                    scope.line.insertBefore(e_item,scope.items[0]);
                    scope.items = scope.line.getElementsByClassName('item');
                },400);
            };

            scope.btnLeft.addEventListener('click', function(e){
                scope.goLeft();
            });

            scope.btnRight.addEventListener('click', function(e){
                scope.goRight();
            });

            init();
        }
    }
});

app.directive('ngWork', function(rContent, rGallery){
    return {
        restrict: 'EA',
        scope: {},
        link: function(scope, e,attrs){
            console.info('ngWork 0 => ', e[0]);

            var galleries = {};
            var isDone = false;
            var e_popup = document.getElementById('work-popup');
            var is_anima = false;

            e_popup.parentNode.removeChild(e_popup);
            document.body.appendChild(e_popup);
            var scrollPos = 0;

            var e_popup_wrap = e_popup.getElementsByClassName('wp-wrap')[0];
            var e_popup_title = e_popup.getElementsByClassName('wp-title')[0];
            var e_popup_text = e_popup.getElementsByClassName('wp-text')[0];
            var e_popup_items = e_popup.getElementsByClassName('wp-items')[0];
            var e_popup_items_wrap = e_popup.getElementsByClassName('wp-items-wrap')[0];
            var e_x = e_popup.getElementsByClassName('x')[0];
            var e_left = e_popup.getElementsByClassName('wp-left')[0];
            var e_right = e_popup.getElementsByClassName('wp-right')[0];

            var items = e[0].getElementsByClassName('item');
            var loaded_galleries_count = 0;
            console.info('ngWork items => ', items);



            var init = function(){

                for(var i=0;i<items.length;i++){
                    items[i].addEventListener('click', function(ev){
                        console.info('ngWork-click - ', ev);

                        var eItem = ev.target;
                        while(eItem.parentNode!=document.body && !eItem.classList.contains('item')){
                            eItem = eItem.parentNode;
                        }

                        var gID = eItem.getAttribute('gallery-id');
                        var gTitle = eItem.getAttribute('gallery-title');
                        var index = eItem.getAttribute('index');

                        console.info('--> ', items[index]);


                        // e_popup.classList.add('transition');
                        e_popup.classList.add('show');

                        console.info('-->> GALLERIES = ', galleries, gID, galleries[gID],galleries[gID].items.length);

                        e_popup_title.innerText = gTitle;
                        // e_popup_text.innerText = galleries[gID].text;
                        e_popup_items_wrap.innerHTML = '';
                        e_popup_items_wrap.style.width = ( galleries[gID].items.length*204 + 10 * (galleries[gID].items.length) ) + 'px';

                        if(galleries[gID].items.length<=4){
                            // e_next.style.display = 'none';
                            e_right.style.display = 'none';
                            e_left.style.display = 'none';
                            // e_prev.style.display = 'none';
                        }

                        for(var ii=0;ii<galleries[gID].items.length;ii++){
                            var e_img = document.createElement('DIV');
                            e_img.style.backgroundImage = 'url('+galleries[gID].items[ii].img+')';
                            e_img.classList.add('wp-item');

                            var e_a = document.createElement('A');
                            e_a.href = galleries[gID].items[ii].img;
                            e_a.setAttribute('rel', "group1");
                            e_a.classList.add('f-img');

                            e_img.appendChild(e_a);

                            e_popup_items_wrap.appendChild(e_img);
                        }

                        // document.body.classList.add('hide-sections');
                        // scrollPos = document.body.scrollTop;


                        $(".f-img").fancybox({
                            'transitionIn'	:	'elastic',
                            'transitionOut'	:	'elastic',
                            'autoDimensions'	: true,
                            'speedIn'		:	600,
                            'speedOut'		:	200,
                            'overlayShow'	:	true
                        });

                    });

                    var gID = items[i].getAttribute('gallery-id');

                    rGallery.get({id:gID}, function(data){
                        console.info('ngWork -> -> ', data.item, galleries);
                        loaded_galleries_count++;

                        // if(data.item != undefined && galleries[data.item.id]==undefined){
                        if(data.item != undefined){
                            galleries[data.item.id] = data.item;
                            console.info('ngWork -> -> -> ', galleries);
                        }else {
                            console.error('ngWork 1 -> not LOADED');
                        }

                        if(loaded_galleries_count == items.length){
                            go();
                        }
                    });
                }


            };

            var go = function(){


                e_popup.addEventListener('click', function(ev){
                    console.info('ngWork-click - e_popup - ', ev);
                    if(ev.target == e_popup){
                        close();
                    }
                });

                e_x.addEventListener('click', function(ev){
                    console.info('ngWork-click - e_x - ', ev);

                    close();
                });

                e_right.addEventListener('click', function(ev){
                    console.info('ngWork-click - e_next - ', ev);

                    if(!is_anima){
                        e_popup_items_wrap.classList.remove('transition');
                        e_popup_items_wrap.style.marginLeft = - (204 + 10) + 'px';
                        var imgs = e_popup_items_wrap.getElementsByClassName('wp-item');
                        var img = imgs[imgs.length-1];
                        e_popup_items_wrap.removeChild(img);
                        e_popup_items_wrap.insertBefore(img,imgs[0]);
                        // e_popup_items_wrap.classList.add('transition');

                        setTimeout(function(){
                            e_popup_items_wrap.classList.add('transition');
                            e_popup_items_wrap.style.marginLeft = '0px';
                            is_anima = true;

                            setTimeout(function(){
                                e_popup_items_wrap.classList.remove('transition');

                                is_anima = false;
                            },400);
                        },30);

                    }
                });

                e_left.addEventListener('click', function(ev){
                    console.info('ngWork-click - e_prev - ', ev);

                    if(!is_anima){
                        e_popup_items_wrap.classList.add('transition');
                        e_popup_items_wrap.style.marginLeft = - (204 + 10) + 'px';
                        is_anima = true;

                        setTimeout(function(){
                            e_popup_items_wrap.classList.remove('transition');
                            e_popup_items_wrap.style.marginLeft = '0px';

                            var imgs = e_popup_items_wrap.getElementsByClassName('wp-item');
                            var img = imgs[0];
                            e_popup_items_wrap.removeChild(imgs[0]);
                            e_popup_items_wrap.appendChild(img);

                            is_anima = false;
                        },400);

                    }


                });
            };

            var close = function(){
                e_popup.classList.add('transition');
                e_popup.classList.remove('show');
                // document.body.classList.remove('hide-sections');
                // document.body.scrollTop = scrollPos;
            };

            init();
        }
    }
});

app.directive('ngGallery', function(rContent, rGallery){
    return {
        restrict: 'EA',
        scope: {
            galleryId: '='
        },
        link: function(scope, e, attrs){
            console.info('ngGallery => ', e[0]);
            scope.w = 0;
            scope.f = true;

            scope.el = e[0].getElementsByClassName('imgs')[0];

            var init = function(){
                scope.els = scope.el.getElementsByClassName('img');
                console.info('ngGallery - init => ', scope.els);

                var w = 0;
                var f =true;

                for(var i=0;i<scope.els.length;i++){
                    scope.els[i].style.left = w+'px';
                    w += scope.els[i].offsetWidth+1;

                    if(scope.els[i].offsetWidth<20){
                        f = false;
                        setTimeout(function(){
                            init();
                        }, 200);
                        break;
                    }
                }

                scope.el.style.width = w+'px';
                scope.w = w;

                if(f){
                    scope.go();
                }

            };

            scope.go = function(){
                //console.info('ngGallery - go => ', scope.els);

                if(scope.f){
                    for(var i=0;i<scope.els.length;i++){
                        if(scope.els[i].offsetLeft+scope.els[i].offsetWidth<0){
                            scope.els[i].style.left = (scope.w-scope.els[i].offsetWidth)+'px';
                        }else{
                            scope.els[i].style.left = (scope.els[i].offsetLeft-1)+'px';
                        }
                    }
                }

                setTimeout(function(){
                    scope.go();
                }, 10);
            };

            scope.el.addEventListener('mouseenter', function(e){
                scope.f = false;
            });

            scope.el.addEventListener('mouseleave', function(e){
                scope.f = true;
            });

            init();
        }
    }
});

app.directive('ngSlider', function(){
    return {
        restrict: 'A',
        transclude: false,
        scope: {
            onLeft: '=',
            onRight: '='
        },
        link: function(scope, e, attrs){
            console.info('ngSlider is ready!', e);

            //scope.slider = document.getElementsByClassName();
            window.slider = e[0];
            scope.slider = e[0];
            scope.k = 0;

            scope.slides = scope.slider.getElementsByClassName('slide');
            scope.n = scope.slides.length;



            // Set slider dimentions like current slide dimensions
            scope.setSliderDim = function(){
                scope.slider.style.height = scope.slides[scope.k].offsetHeight + 'px';
            };

            scope.showActiveSlide = function(){
                for(var i=0; i<scope.n; i++){
                    if(i==scope.k){
                        //scope.slides[i].style.opacity = 1;
                        //scope.slides[i].style.display = 'block';
                        if(!scope.slides[i].classList.contains('active')) scope.slides[i].classList.add('active')
                    }else{
                        //scope.slides[i].style.opacity = 0;
                        if(scope.slides[i].classList.contains('active')) scope.slides[i].classList.remove('active');
                        //scope.slides[i].style.display = 'none';
                        //setTimeout(function(){
                        //    scope.slides[i].style.display = 'none';
                        //}, 400);
                    }
                }
                scope.setSliderDim();
            };
            scope.showActiveSlide();






            scope.$on('onLeft', function(){
                console.log('Slide left!');

                if(scope.k>0){
                    scope.k--;
                }else{
                    scope.k = scope.n-1;
                }

                scope.showActiveSlide();
            });

            scope.$on('onRight', function(){
                console.log('Slide right!');

                if(scope.k<scope.n-1){
                    scope.k++;
                }else{
                    scope.k = 0;
                }

                scope.showActiveSlide();
            });
        }
    }
});

app.directive('onSizeChanged', ['$window', function ($window) {
    return {
        restrict: 'A',
        scope: {
            onSizeChanged: '&'
        },
        link: function (scope, $element, attr) {
            var element = $element[0];

            cacheElementSize(scope, element);
            $window.addEventListener('resize', onWindowResize);

            function cacheElementSize(scope, element) {
                scope.cachedElementWidth = element.offsetWidth;
                scope.cachedElementHeight = element.offsetHeight;
            }

            function onWindowResize() {
                var isSizeChanged = scope.cachedElementWidth != element.offsetWidth || scope.cachedElementHeight != element.offsetHeight;
                if (isSizeChanged) {
                    var expression = scope.onSizeChanged(element.offsetWidth, element.offsetHeight);
                    expression();
                }
            };
        }
    }
}]);
/**
 * Created by emaxe on 13.07.2015.
 */

app.factory('rContent',['$resource', 'Config', function($resource, Config){
    return $resource('publicApiContent/:id', null, {
        query: {
            method: 'GET',
            isArray: false
        },
        addToast: {
            method:'POST',
            url: 'publicApiAddToast',
            isArray: false
        }
    });
}]);

app.factory('rGallery',['$resource', 'Config', function($resource, Config){
    return $resource('publicApiGallery/:id', null, {
        query: {
            method: 'GET',
            isArray: false
        }
    });
}]);

app.factory('rSms',['$resource', 'Config', function($resource, Config){
    return $resource('publicApiSms/:id', null, {
        send: {
            method:'POST',
            url: 'publicApiSmsSend',
            isArray: false
        }
    });
}]);
//# sourceMappingURL=ngApp-theme.js.map
