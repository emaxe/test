/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('BlockCtrl', ['$scope', 'app', 'Config', 'rContent', 'Storage', 'helper',
    function($scope, app, Config, rContent, Storage, helper){
        console.log('blockCtrl is ready!');
        var that = this;
        $scope.app = app;

        this.fLoad = false;
        this.items = null;

        //========================

        this.clear = function(){
            this.fLoad = false;
            this.items = null;
            that.items = null;
        };

        this.init = function(f){
            that.clear();

            Storage.initBlock = that.init;
            Storage.clearBlock = that.clear;


            Storage.topMenuItems = [];
            if(Storage.block.tabs.length>1){
                Storage.block.tabs.forEach(function(item,i){
                    Storage.topMenuItems.push({
                        tooltip: item.title,
                        label: item.name,
                        name: item.name,
                        type: 'flat',
                        text: item.title,
                        onClick: function(){
                            that.setItemsGroup({
                                key: 'group',
                                value: 'block-'+Storage.block.name+'-'+item.name
                            });
                            that.items = null;
                            that.init_items();
                            Storage.headerTabActive = item.name;
                        }
                    });
                });
            }
            Storage.topMenuItems.push({
                icon: 'ic_save_white_24px.svg',
                tooltip: 'Сохранить',
                label: 'Save',
                onClick: function(){
                    that.save();
                }
            });
            Storage.topMenuItems.push({
                icon: 'ic_replay_white_24px.svg',
                tooltip: 'Обновить',
                label: 'Refresh',
                onClick: function(){
                    that.reload();
                }
            });

            app.screen.current.title = 'Блок - '+Storage.block.title;
            Storage.headerTabActive = Storage.block.tabs[0].name;
            that.setItemsGroup({
                key: 'group',
                value: 'block-'+Storage.block.name+'-'+Storage.block.tabs[0].name
            });
            console.info('init', that.fSave);

            that.init_items(f);
        };

        this.init_items = function(f){
            that.fLoad = true;
            rContent.query({key: that.getItemsGroup().key, value: that.getItemsGroup().value}, function(data){
                that.fLoad = false;
                that.items = data.items;
                if(typeof(f)=='function'){
                    f(data);
                }
                console.info('rContent.query - items', data);
            });
        };

        this.reload = function(){
            that.init_items(function(data){
                if(helper.isObjectTrue(that.fSave)) helper.showToast('Обновлено');
            });
        };

        this.save = function(){
            that.save_items();
        };

        this.save_items = function(f){
            var sItems = [];
            for(var i=0;i<that.items.length;i++){
                var sItem = angular.copy(that.items[i]);
                sItem.value = JSON.stringify(sItem.value);
                sItems.push(sItem);
            }
            that.fLoad = true;
            rContent.save(sItems, function(data){
                that.fLoad = false;
                helper.showToast('Сохранено');
                if(typeof(f)=='function'){
                    f(data);
                }
                console.info('rContent.update - items', data);
            });
        };


        this.getItemsGroup = function(){
            return Storage.block_itemGroup;
        };

        this.setItemsGroup = function(s){
            Storage.block_itemGroup = s;
        };


        this.init();

    }]);
//# sourceMappingURL=blockCtrl.js.map
