/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('MenuCtrl', ['$scope', 'app', 'Config', 'rContent', 'Storage', 'helper',
    function($scope, app, Config, rContent, Storage, helper){
        console.log('menuCtrl is ready!');
        var that = this;
        $scope.app = app;

        this.fLoad = false;
        this.items = Storage.menu;

        //========================
        this.menuItems = [
            {
                text: 'Данные',
                tooltip: 'Данные',
                label: 'data',
                name: 'data',
                type: 'flat',
                onClick: function(){
                    that.setItemsGroup({
                        key: 'group',
                        value: 'block-menu-data'
                    });
                    that.items = null;
                    that.init_items();
                    Storage.headerTabActive = 'data';
                }
            },
            {
                icon: 'ic_settings_white_24px.svg',
                tooltip: 'Настройки',
                label: 'Settings',
                name: 'settings',
                type: 'flat',
                onClick: function(){
                    that.setItemsGroup({
                        key: 'group',
                        value: 'block-menu-settings'
                    });
                    that.items = null;
                    that.init_items();
                    Storage.headerTabActive = 'settings';
                }
            },
            {
                icon: 'ic_save_white_24px.svg',
                tooltip: 'Сохранить',
                label: 'Save',
                onClick: function(){
                    that.save();
                }
            },
            {
                icon: 'ic_replay_white_24px.svg',
                tooltip: 'Обновить',
                label: 'Refresh',
                onClick: function(){
                    that.reload();
                }
            }
        ];

        Storage.topMenuItems = this.menuItems;
        //========================

        this.init = function(f){
            Storage.headerTabActive = 'data';
            that.setItemsGroup({
                key: 'group',
                value: 'block-menu-data'
            });
            console.info('init', that.fSave);

            that.init_items(f);
        };

        this.init_items = function(f){
            that.fLoad = true;
            rContent.query({key: that.getItemsGroup().key, value: that.getItemsGroup().value}, function(data){
                that.fLoad = false;
                //that.fSave.items = true;
                that.items = data.items;
                if(typeof(f)=='function'){
                    f(data);
                }
                console.info('rContent.query - items', data);
            });
        };

        this.reload = function(){
            that.init_items(function(data){
                if(helper.isObjectTrue(that.fSave)) helper.showToast('Обновлено');
            });
        };

        this.save = function(){
            that.save_items();
        };

        this.save_items = function(f){
            var sItems = [];
            for(var i=0;i<that.items.length;i++){
                var sItem = angular.copy(that.items[i]);
                sItem.value = JSON.stringify(sItem.value);
                sItems.push(sItem);
            }
            that.fLoad = true;
            rContent.save(sItems, function(data){
                that.fLoad = false;
                helper.showToast('Сохранено');
                if(typeof(f)=='function'){
                    f(data);
                }
                console.info('rContent.update - items', data);
            });
        };


        this.getItemsGroup = function(){
            return Storage.block_menu_itemGroup;
        };

        this.setItemsGroup = function(s){
            Storage.block_menu_itemGroup = s;
        };


        this.init();

    }]);
//# sourceMappingURL=menuCtrl.js.map
