/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('SettingsCtrl', ['$scope', 'app', 'Config', 'rContent', 'rContentField', 'rContentType', 'Storage', 'helper', function($scope, app, Config, rContent, rContentField, rContentType, Storage, helper){
    console.log('SettingsCtrl is ready!');
    var that = this;
    $scope.app = app;

    this.info = null;
    this.fLoad = false;
    this.page = undefined;

    this.types = undefined;
    this.contents = undefined;
    this.fields = undefined;

    this.showAddBtn = false;

    this.filter = {
        type: {
            name: '',
            title: '',
        },
        content: {
            id: ''
        },
        type_id: 0,
        group_type_id: 0
    };

    this.query = {
        types:{
            limit: 5,
            page: 1
        },
        fields:{
            limit: 5,
            page: 1
        },
        content:{
            limit: 5,
            page: 1
        }
    };

    //========================
    this.menuItems = [
        {
            tooltip: 'Типы',
            label: 'Типы',
            name: 'types',
            type: 'flat',
            text: 'Типы',
            onClick: function(){
                that.initTypes();
            }
        },
        {
            tooltip: 'Поля',
            label: 'Поля',
            name: 'fields',
            type: 'flat',
            text: 'Поля',
            onClick: function(){
                that.initFields();
            }
        },
        {
            tooltip: 'Блоки',
            label: 'Блоки',
            name: 'contents',
            type: 'fields',
            text: 'Блоки',
            onClick: function(){
                that.initContents();
            }
        },
        {
            icon: 'ic_add_white_24px.svg',
            tooltip: 'Добавить',
            label: 'Add',
            onClick: function(ev){
                that.add(ev);
            }
        },
        {
            icon: 'ic_save_white_24px.svg',
            tooltip: 'Сохранить',
            label: 'Save',
            onClick: function(ev){
                that.save();
            }
        },
        {
            icon: 'ic_replay_white_24px.svg',
            tooltip: 'Обновить',
            label: 'Refresh',
            onClick: function(ev){
                that.reload();
            }
        }
    ];

    Storage.topMenuItems = this.menuItems;


    //========================

    this.init = function(f){
        if(that.page=='types' || that.page==undefined) that.initTypes();
        if(that.page=='fields') that.initFields();
        if(that.page=='contents') that.initContents();
    };

    this.initTypes = function(){
        that.page = 'types';
        Storage.headerTabActive = 'types';
        that.getTypes();
    };

    this.initFields = function(){
        that.page = 'fields';
        Storage.headerTabActive = 'fields';
        //that.getFields();
        that.filterType();
    };

    this.initContents = function(){
        that.page = 'contents';
        Storage.headerTabActive = 'contents';
        that.filterType();
    };


    this.reload = function(){
        that.init(function(data){
            helper.showToast('Обновлено');
        });
    };

    this.save = function(){
        var sItem = angular.copy(that.item);
        sItem.value = JSON.stringify(that.item.value);
        that.fLoad = true;
        rContent.update({id:sItem.id}, sItem, function(data){
            that.change = true;
            that.fLoad = false;
            helper.showToast('Сохранено');
            console.info('rContent.update', data);
        });
    };


    this.getTypes = function(o, f){
        that.fLoad = true;
        if(o==undefined) o = {};
        o.prepare = true;

        rContentType.query(o, function(data){
            that.fLoad = false;
            that.types = data.items;
            if(typeof(f)=='function'){
                f(data);
            }
            console.info('rContentType.query', data);
        });
    };

    this.getFields = function(o, f){
        that.fLoad = true;
        if(o==undefined) o = {};
        o.prepare = true;

        rContentField.query(o, function(data){
            that.fLoad = false;
            that.fields = data.items;
            if(typeof(f)=='function'){
                f(data);
            }
            console.info('rContentField.query', data);
        });
    };

    this.getContents = function(o, f){
        that.fLoad = true;
        if(o==undefined) o = {};
        o.prepare = true;

        rContent.query(o, function(data){
            that.fLoad = false;
            that.contents = data.items;
            if(typeof(f)=='function'){
                f(data);
            }
            console.info('rContent.query', data);
        });
    };


    this.add = function(ev){
        if(that.page == 'fields'){
            that.addField(ev);
        }
        if(that.page == 'types'){
            that.addType(ev);
        }
        if(that.page == 'contents'){
            //that.addContent(ev);
        }
    };


    this.addType = function(ev){
        helper.showDialog({
            ev: ev,
            controllerName: 'dialogCreateType',
            tmpName: 'createType',
            success: function(data){
                console.info('addType - success -', data);
                that.fLoad = true;
                rContentType.create(data.item, function(data){
                    console.info('addType - success - rContentType.create', data);
                    helper.showToast('Тип создан');
                    that.initTypes();
                });
            },
            error: function(){
                console.error('addType - error');
            },
            cancel: function(){
                console.info('addType - cancel');
            }
        });
    };

    this.addField = function(ev){
        helper.showDialog({
            ev: ev,
            controllerName: 'dialogCreateField',
            tmpName: 'createField',
            inScope: {
                types: that.types,
                filter: that.filter
            },
            success: function(data){
                console.info('addField - success -', data);
                that.fLoad = true;
                rContentField.create(data.item, function(data){
                    console.info('addField - success - rContentField.create', data);
                    helper.showToast('Поле создано');
                    that.initFields();
                });
            },
            error: function(){
                console.error('addField - error');
            },
            cancel: function(){
                console.info('addField - cancel');
            }
        });
    };

    this.addContent = function(ev){
        helper.showDialog({
            ev: ev,
            controllerName: 'dialogCreateContent',
            tmpName: 'createContent',
            inScope: {
                filter: that.filter,
                types: that.types
            },
            success: function(data){
                console.info('addContent - success -', data);
                that.fLoad = true;
                rContent.create2(data.item, function(data){
                    console.info('addContent - success - rContent.create', data);
                    helper.showToast('Блок создан');
                    that.initContents();
                });
            },
            error: function(){
                console.error('addContent - error');
            },
            cancel: function(){
                console.info('addContent - cancel');
            }
        });
    };




    this.editType = function(item, ev){
        helper.showDialog({
            ev: ev,
            controllerName: 'dialogEditType',
            tmpName: 'editType',
            inScope: {
                item: item
            },
            success: function(data){
                console.info('editType - success -', data);
                that.fLoad = true;
                rContentType.save2(data.item, function(data){
                    that.fLoad = false;
                    console.info('editType - success - rContentType.update', data);
                    helper.showToast('Тип изменен');
                    //that.filterType();
                });
            },
            error: function(){
                console.error('editType - error');
            },
            cancel: function(){
                console.info('editType - cancel');
            }
        });
    };

    this.editField = function(item, ev){
        helper.showDialog({
            ev: ev,
            controllerName: 'dialogEditField',
            tmpName: 'editField',
            inScope: {
                item: item,
                types: that.types
            },
            success: function(data){
                console.info('editField - success -', data);
                that.fLoad = true;
                rContentField.save(data.item, function(data){
                    that.fLoad = false;
                    console.info('editField - success - rContentField.update', data);
                    helper.showToast('Поле изменено');
                    //that.filterType();
                });
            },
            error: function(){
                console.error('editField - error');
            },
            cancel: function(){
                console.info('editField - cancel');
            }
        });
    };

    this.editContent = function(item, ev){
        helper.showDialog({
            ev: ev,
            controllerName: 'dialogEditContent',
            tmpName: 'editContent',
            inScope: {
                item: item
            },
            success: function(data){
                console.info('editContent - success -', data);
                that.fLoad = true;
                rContent.save2({items: [data.item]}, function(data){
                    that.fLoad = false;
                    console.info('editContent - success - rContent.update', data);
                    helper.showToast('Блок изменен');
                    //that.filterType();
                });
            },
            error: function(){
                console.error('editContent - error');
            },
            cancel: function(){
                console.info('editContent - cancel');
            }
        });
    };



    this.deleteType = function(item, ev){
        helper.confirm({
            title: 'Удаление типа',
            text: 'Точно хотите удалить тип ['+item.title+']?',
            yes: {
                title: 'Удалить',
                onClick: function(){
                    that.fLoad = true;
                    rContentType.delete({id: item.id}, function(data){
                        console.info('rContentType.delete', data);
                        helper.showToast('Тип ['+item.title+'] удален');
                        that.initTypes();
                    });
                }
            },
            no: {
                title: 'Отмена'
            },
            event: ev
        });
    };

    this.deleteField = function(item, ev){
        helper.confirm({
            title: 'Удаление поля',
            text: 'Точно хотите удалить поле ['+item.title+']?',
            yes: {
                title: 'Удалить',
                onClick: function(){
                    that.fLoad = true;
                    rContentField.delete({id: item.id}, function(data){
                        console.info('rContentField.delete', data);
                        helper.showToast('Поле ['+item.title+'] удалено');
                        that.initFields()
                    });
                }
            },
            no: {
                title: 'Отмена'
            },
            event: ev
        });
    };







    this.showFieldsByType = function(item, ev){
        that.filter.type_id = item.id;
        that.initFields();
    };

    this.showTypeByID = function(id, ev){
        var type = that.getTypeById(id);
        that.filter.type.id = id;
        that.filter.type.name = type.name;
        that.filter.type.title = type.title;
        that.initTypes();
    };

    this.showContentsByID = function(id, ev){
        that.filter.content.id = id;
        that.filter.type_id = 0;
        that.initContents();
    };

    this.showContentsByType = function(item, ev){
        that.filter.type_id = item.id;
        that.initContents();
    };





    this.filterType = function(){
        if(that.page=='fields'){
            that.fields = undefined;
            var o = {};
            if(that.filter.type_id>0){
                o = {
                    key: 'content_type_id',
                    value: that.filter.type_id
                }
            }
            that.getFields(o)
        }

        if(that.page=='contents'){
            that.contents = undefined;
            var o = {};
            if(that.filter.type_id>0){
                o = {
                    key: 'type_id',
                    value: that.filter.type_id
                }
            }
            that.getContents(o)
        }

    };



    this.getTypeById = function(id){
        var type = undefined;
        for(var i in that.types){
            if(id == that.types[i].id){
                type = that.types[i];
                break;
            }
        }
        return type;
    };

    this.getContentById = function(id){
        var content = undefined;
        for(var i in that.contents){
            if(id == that.contents[i].id){
                content = that.contents[i];
                break;
            }
        }
        return content;
    };


    this.clearTypeFilter = function(){
        that.filter.type={};
    };


    that.init();

}]);
//# sourceMappingURL=settingsCtrl.js.map
