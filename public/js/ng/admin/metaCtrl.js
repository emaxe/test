/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('MetaCtrl', ['$scope', 'app', 'Config', 'rContent', 'Storage', 'helper', function($scope, app, Config, rContent, Storage, helper){
    console.log('metaCtrl is ready!');
    var that = this;
    $scope.app = app;

    this.fLoad = false;
    this.item = Storage.meta;
    this.change = false;

    //========================
    this.menuItems = [
        {
            icon: 'ic_save_white_24px.svg',
            tooltip: 'Сохранить',
            label: 'Save',
            onClick: function(){
                that.save();
            }
        },
        {
            icon: 'ic_replay_white_24px.svg',
            tooltip: 'Обновить',
            label: 'Refresh',
            onClick: function(){
                that.init(function(data){
                    helper.showToast('Обновлено');
                });
            }
        }
    ];

    Storage.topMenuItems = this.menuItems;
    //========================

    this.init = function(f){
        that.fLoad = true;
        rContent.query({key: 'name', value: 'meta'}, function(data){
            that.fLoad = false;
            Storage.meta = data.items[0];
            that.item = data.items[0];
            that.change = true;
            if(typeof(f)=='function'){
                f(data);
            }
            console.info('rContent.query', data);
        });
    };

    this.reload = function(){
        that.init(function(data){
            helper.showToast('Обновлено');
        });
    };

    this.save = function(){
        var sItem = angular.copy(that.item);
        sItem.value = JSON.stringify(that.item.value);
        that.fLoad = true;
        rContent.update({id:sItem.id}, sItem, function(data){
            that.change = true;
            that.fLoad = false;
            helper.showToast('Сохранено');
            console.info('rContent.update', data);
        });
    };




    this.init();

}]);
//# sourceMappingURL=metaCtrl.js.map
