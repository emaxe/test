/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('GalleriesCtrl', ['$q', '$scope', '$timeout', 'app', 'Config', 'rGallery', 'Storage', 'helper',
    function($q, $scope, $timeout, app, Config, rGallery, Storage, helper){
        console.log('GalleriesCtrl is ready!');
        var that = this;
        $scope.app = app;

        this.fLoad = false;
        this.items = null;
        this.item = null;
        this.fList = true;

        //========================
        this.menuItems = [
            {
                icon: 'ic_add_white_24px.svg',
                tooltip: 'Добавить',
                label: 'Add',
                onClick: function(ev){
                    that.addGallery(ev)
                }
            },
            {
                icon: 'ic_replay_white_24px.svg',
                tooltip: 'Обновить',
                label: 'Refresh',
                onClick: function(){
                    that.init(function(data){
                        helper.showToast('Обновлено');
                    });
                }
            }
        ];

        this.menuItems2 = [
            {
                icon: 'ic_keyboard_arrow_left_white_24px.svg',
                tooltip: 'Назад',
                label: 'Back',
                onClick: function(){
                    that.back();
                }
            },
            {
                icon: 'ic_save_white_24px.svg',
                tooltip: 'Сохранить',
                label: 'Save',
                onClick: function(){
                    that.save();
                }
            },
            {
                icon: 'ic_replay_white_24px.svg',
                tooltip: 'Обновить',
                label: 'Refresh',
                onClick: function(){
                    that.initModel(function(data){
                        helper.showToast('Обновлено2');
                    });
                }
            }
        ];

        Storage.topMenuItems = this.menuItems;
        //========================

        this.init = function(f){
            that.fLoad = true;
            rGallery.query(function(data){
                that.fLoad = false;
                that.items = data.items;
                if(typeof(f)=='function'){
                    f(data);
                }
                console.info('rContent.query', data);
            });
        };

        this.initModel = function(f){
            that.fLoad = true;
            rGallery.get({id:that.item.id},function(data){
                that.fLoad = false;
                that.item = data.item;
                that.RFMInit(that.item);
                if(typeof(f)=='function'){
                    f(data);
                }
                console.info('rContent.get', data);
            });
        };

        this.addGallery = function(ev){
            helper.showDialog({
                ev: ev,
                controllerName: 'dialogCreateGallery',
                tmpName: 'createGallery',
                success: function(data){
                    console.info('addGallery - success -', data);
                    that.fLoad = true;
                    rGallery.create({item: data.item}, function(data){
                        console.info('addGallery - success - rGallery.create', data);
                        helper.showToast('Галлерея создана');
                        that.init();
                    });
                },
                error: function(){
                    console.error('addGallery - error');
                },
                cancel: function(){
                    console.info('addGallery - cancel');
                }
            });
        };

        this.deleteGallery = function(gallery, ev){
            helper.confirm({
                title: 'Удаление галлереи',
                text: 'Точно хотите удалить галлерею ['+gallery.title+']?',
                yes: {
                    title: 'Удалить',
                    onClick: function(){
                        that.fLoad = true;
                        rGallery.delete({id: gallery.id}, function(data){
                            console.info('rGallery.delete', data);
                            helper.showToast('Галлерея ['+gallery.title+'] удалена');
                            that.init();
                        });
                    }
                },
                no: {
                    title: 'Отмена'
                },
                event: ev
            });
        };

        this.editGallery = function(gallery, ev){
            console.info('this.editGallery', gallery);
            Storage.topMenuItems = that.menuItems2;
            that.item = gallery;
            that.fList = false;

            app.screen.current.title = 'Галлерея - '+that.item.title;

            that.RFMInit(that.item);
        };

        this.changePublished = function(item, ev){
            that.fLoad = true;
            rGallery.update({id: item.id, item: item}, function(data){
                console.info('rGallery.update', data);
                var s = '';
                if(item.published=='1') s= 'опубликованна';
                if(item.published=='0') s= 'снята с публикации';
                helper.showToast('Галлерея ['+item.title+'] '+s);
                that.fLoad = false;
            });
        };

        that.back = function(){
            console.info('this.back');
            Storage.topMenuItems = that.menuItems;
            that.item = null;
            that.fList = true;

            app.screen.current.title = 'Галлереи';

            that.init(function(){
               //
            });
        };


        this.save = function(){
            that.fLoad = true;
            var item = that.item;
            rGallery.update({id: item.id, item: item}, function(data){
                console.info('rGallery.update', data);
                helper.showToast('Галлерея ['+item.title+'] сохранена');
                that.fLoad = false;
            });
        };


        this.deleteImg = function(ev, item){
            helper.confirm({
                title: 'Удаление картинки',
                text: 'Точно хотите удалить картинку?',
                yes: {
                    title: 'Удалить',
                    onClick: function(){
                        that.fLoad = true;
                        rGallery.deleteImg({item: item}, function(data){
                            console.info('rContent.deleteImg - items', data);
                            that.fLoad = false;
                            helper.showToast('Картинка удалена!');
                            that.initModel();
                        });
                    }
                },
                no: {
                    title: 'Отмена'
                },
                event: ev
            });
        };


        this.addImg = function(ev){
            helper.confirm({
                title: 'Создание новой картинки',
                text: 'Точно хотите создать новую картинку?',
                yes: {
                    title: 'Создать',
                    onClick: function(){
                        that.fLoad = true;
                        var newItem = {
                            gallery_id: that.item.id,
                            title: '',
                            description: '',
                            img: '',
                            order: 100,
                            published: '0'
                        };
                        rGallery.createImg({item:newItem}, function(data){
                            console.info('rContent.createImg - items', data);
                            that.fLoad = false;
                            helper.showToast('Элемент создан!');
                            that.initModel();
                        });
                    }
                },
                no: {
                    title: 'Отмена'
                },
                event: ev
            });
        };


        this.RFMInit = function(item){

            console.info("F");
            $('.iframe-btn').fancybox({
                'width'		: 900,
                'height'	: 600,
                'type'		: 'iframe',
                'autoScale'    	: false
            });

            setTimeout(function(){
                console.info("F");
                $('.iframe-btn').fancybox({
                    'width'		: 900,
                    'height'	: 600,
                    'type'		: 'iframe',
                    'autoScale'    	: false
                });
            }, 1000);

            var f = function(field_id){
                var o = document.getElementById(field_id);
                var url = o.value;
                for(var i=0; i<item.items.length;i++){
                    var field = item.items[i];
                    if(field_id == 'imgField_'+item.items[i].id+'_'+i){
                        item.items[i].img = url;
                        console.log('Catch!!! ', field_id, o.value, field);
                        $scope.$apply();
                        break;

                    }
                }
                console.log('Catch! ', field_id, o.value, item);
            };

            for(var i=0; i<item.items.length;i++){

                console.log('--> ADDED ', item.items[i].id+'_'+i);
                RFMCallbacks.push({
                    id: item.items[i].id+'_'+i,
                    go: f
                });

            }
        };







        $scope.query = {
            order: 'order',
            limit: 5,
            page: 1
        };

        $scope.onpagechange = function(page, limit) {
            var deferred = $q.defer();

            $timeout(function () {
                deferred.resolve();
            }, 200);

            return deferred.promise;
        };

        $scope.onorderchange = function(order) {
            var deferred = $q.defer();

            $timeout(function () {
                deferred.resolve();
            }, 200);

            return deferred.promise;
        };







        this.init();

    }]);
//# sourceMappingURL=galleriesCtrl.js.map
