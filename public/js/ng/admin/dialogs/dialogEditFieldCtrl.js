/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('DialogEditFieldCtrl', function($scope, $mdDialog, rContent, rContentField, helper, Storage){
    console.log('dialogEditFieldCtrl is ready!');
    var that = this;
    $scope.app = app;
    $scope.Storage = Storage;
    this.InScope = $scope.InScope;

    this.items = null;
    //this.field = $scope.InScope.field;
    this.fLoad = true;

    this.item = that.InScope.item;
    this.types = that.InScope.types;

    this.init = function(){

        rContent.query(o, function(data){
            that.fLoad = false;
            that.contents = data.items;
            if(typeof(f)=='function'){
                f(data);
            }
            console.info('rContent.query', data);
        });
    };

    this.confirm = function(){
        var f = true;
        var s = '';
        if(that.item.name==''){
            f = false;
            s += '[Имя] ';
        }
        if(that.item.title==''){
            f = false;
            s += '[Заголовок] ';
        }
        if(f) {
            that.answer(that.item)
        }else{
            helper.showToast( 'Поля '+s+' обязательны для заполнения!');
        }
    };

    this.hide = function() {
        $mdDialog.hide();
    };
    this.cancel = function() {
        $mdDialog.cancel();
    };
    this.answer = function(answer) {
        $mdDialog.hide({
            state:'ok',
            item: answer
        });
    };

    that.init();

});
//# sourceMappingURL=dialogEditFieldCtrl.js.map
