/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('DialogCreateFieldCtrl', function($scope, $mdDialog, rContent, rContentField, Storage, helper){
    console.log('dialogCreateFieldCtrl is ready!');
    var that = this;
    $scope.app = app;
    $scope.Storage = Storage;
    this.InScope = $scope.InScope;

    this.items = null;
    this.types = $scope.InScope.types;
    this.filter = $scope.InScope.filter;
    this.fLoad = true;

    this.item = {
        content_type_id: that.filter.type_id,
        name: '',
        title: '',
        order: 100,
        published: '0'
    };

    this.init = function(){

    };

    this.confirm = function(){
        var f = true;
        var s = '';
        if(that.item.name==''){
            f = false;
            s += '[Имя] ';
        }
        if(that.item.title==''){
            f = false;
            s += '[Заголовок] ';
        }
        if(f) {
            that.answer(that.item)
        }else{
            helper.showToast( 'Поля '+s+' обязательны для заполнения!');
        }
    };

    this.hide = function() {
        $mdDialog.hide();
    };
    this.cancel = function() {
        $mdDialog.cancel();
    };
    this.answer = function(answer) {
        $mdDialog.hide({
            state:'ok',
            item: answer
        });
    };

    that.init();

});
//# sourceMappingURL=dialogCreateFieldCtrl.js.map
