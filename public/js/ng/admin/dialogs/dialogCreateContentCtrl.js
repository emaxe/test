/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('DialogCreateContentCtrl', function($scope, $mdDialog, rContent, rContentField){
    console.log('dialogCreateContentCtrl is ready!');
    var that = this;
    $scope.app = app;
    this.InScope = $scope.InScope;

    this.contents = undefined;
    this.filter = $scope.InScope.filter;
    this.types = $scope.InScope.types;
    this.fLoad = true;

    this.item = {
        type_id: that.filter.type_id
    };

    this.init = function(){
        that.fLoad = true;
        rContentField.query({prepare: true }, function(data){
            that.fLoad = false;
            that.contents = data.items;
            if(typeof(f)=='function'){
                f(data);
            }
            console.info('rContentField.query - items', data);
        });
    };

    this.confirm = function(){
        var f = true;
        var s = '';
        if(that.item.name==''){
            f = false;
            s += '[���] ';
        }
        if(that.item.title==''){
            f = false;
            s += '[���������] ';
        }
        if(that.item.type_id==0){
            f = false;
            s += '[��� �����] ';
        }
        if(f) {
            that.answer(that.item)
        }else{
            helper.showToast( '���� '+s+' ����������� ��� ����������!');
        }
    };

    this.hide = function() {
        $mdDialog.hide();
    };
    this.cancel = function() {
        $mdDialog.cancel();
    };
    this.answer = function(answer) {
        $mdDialog.hide({
            state:'ok',
            item: answer
        });
    };

    that.init();

});
//# sourceMappingURL=dialogCreateContentCtrl.js.map
