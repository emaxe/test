/**
 * Created by emaxe on 10.07.2015.
 */

var app = angular.module('app', [
    'ngResource',
    'oc.lazyLoad',
    'ui.tinymce',
    'md.data.table',
    'ngMaterial',
    'RecursionHelper',
    //'angularModalService'
], function($interpolateProvider) {
    //$interpolateProvider.startSymbol('<%');
    //$interpolateProvider.endSymbol('%>');
});

app.config(function($sceDelegateProvider, $mdIconProvider, $httpProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        'http://admin.ss-zabor.dev/**',
        'http://ss-zabor.dev/**',
        'http://admin.ss-zabor.tk/**',
        'http://ss-zabor.tk/**',
    ]);
    var csrfToken = $('meta[name=_token]').attr('content');
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = csrfToken;
    $httpProvider.defaults.headers.post['X-CSRF-Token'] = csrfToken;
    $httpProvider.defaults.headers.put['X-CSRF-Token'] = csrfToken;
    $httpProvider.defaults.headers.patch['X-CSRF-Token'] = csrfToken;

    $httpProvider.defaults.headers.common['Content-Type']= 'application/json; charset=UTF-8';
    $httpProvider.defaults.headers.common['App']= 'ng';
});

app.factory('app', function($ocLazyLoad, Storage, Config){
    console.log('app is init!');
    var that = this;

    this.options = {
        mainUrl: Config.mainUrl,
    };

    this.getJS = function(name,f){
        console.log('getJS - INDEX OF => ', Storage.jsLoadArray.indexOf(name));
        if(Storage.jsLoadArray.indexOf(name)<0){
            Storage.fLoad = true;
            console.log('getJS - try load => ', 'js/'+name+'.js');
            $ocLazyLoad.load('js/'+name+'.js').then(function() {
                console.log('LL - success1!', Storage.jsLoadArray);
                Storage.jsLoadArray.push(name);
                Storage.fLoad = false;
                console.log('LL - success2!', Storage.jsLoadArray);
                if(typeof(f)=='function'){
                    f();
                }
            }, function(e) {
                console.log(e);
            })
        }else{
            if(typeof(f)=='function'){
                f();
            }
        }
    };


    this.pages = { // страницы
        home: {
            name: 'home',
            src: 'ng/home.html',
            title: 'Главная',
            onLoad: function(){}
        },
        sms: {
            name: 'sms',
            src: 'ng/sms.html',
            title: 'SMS',
            onLoad: function(){}
        },
        meta: {
            name: 'meta',
            src: 'ng/meta.html',
            title: 'Настройка',
            onLoad: function(){}
        },
        menu: {
            name: 'menu',
            src: 'ng/menu.html',
            title: 'Меню',
            onLoad: function(){}
        },
        block: {
            name: 'block',
            src: 'ng/block.html',
            title: 'Блок',
            onLoad: function(){}
        },
        galleries: {
            name: 'galleries',
            src: 'ng/galleries.html',
            title: 'Галлереи',
            onLoad: function(){}
        },
        blocks: {
            name: 'blocks',
            src: 'ng/blocks.html',
            title: 'Блоки',
            onLoad: function(){}
        },
        settings: {
            name: 'settings',
            src: 'ng/settings.html',
            title: 'Настройки ROOT',
            onLoad: function(){}
        },
        blank: {
            name: 'blank',
            src: 'ng/blank.html',
            title: 'Blank page',
            onLoad: function(){}
        }
    };

    this.screen = {
        current: that.pages.blank,
        load: function(page, f){
            that.screen.preloader = false;
            if (typeof(that.screen.current.onLoad)=='function'){
                that.screen.current.onLoad();
            }
            if (typeof(f)=='function'){
                f();
            }
            console.log('JSLOAD = ',page);
            that.getJS('ng/admin/'+page.name+'Ctrl', function(){
                console.log('app igetJS is success');
                that.screen.current = page;
                if (that.screen.current.name!=page.name){
                    that.screen.preloader = true;
                    that.apply();
                }
            });
        },
        unload: function(){
            that.screen.current = that.pages.blank;
        }
    };


    return this;
});

var RFMCallbacks = [];
window.RFMCallbacks = RFMCallbacks;

function responsive_filemanager_callback(field_id){
    console.log('Callbacks => ', RFMCallbacks, field_id);
    RFMCallbacks.map(function(item){
        if('imgField_'+item.id==field_id){
            item.go(field_id);
        }
    });
}


$('.iframe-btn').fancybox({
    'width'		: 900,
    'height'	: 600,
    'type'		: 'iframe',
    'autoScale'    	: false
});
/**
 * Created by emaxe on 13.07.2015.
 */
angular.module('RecursionHelper', []).factory('RecursionHelper', ['$compile', function($compile){
    return {
        /**
         * Manually compiles the element, fixing the recursion loop.
         * @param element
         * @param [link] A post-link function, or an object with function(s) registered via pre and post properties.
         * @returns An object containing the linking functions.
         */
        compile: function(element, link){
            // Normalize the link parameter
            if(angular.isFunction(link)){
                link = { post: link };
            }

            // Break the recursion loop by removing the contents
            var contents = element.contents().remove();
            var compiledContents;
            return {
                pre: (link && link.pre) ? link.pre : null,
                /**
                 * Compiles and re-adds the contents
                 */
                post: function(scope, element){
                    // Compile the contents
                    if(!compiledContents){
                        compiledContents = $compile(contents);
                    }
                    // Re-add the compiled contents to the element
                    compiledContents(scope, function(clone){
                        element.append(clone);
                    });

                    // Call the post-linking function, if any
                    if(link && link.post){
                        link.post.apply(null, arguments);
                    }
                }
            };
        }
    };
}]);
/**
 * Created by emaxe on 10.07.2015.
 */
//if(app==undefined){
//    var app = angular.module('app', []);
//}

app.controller('mainCtrl', function($scope, app, Storage, $mdSidenav, blocksList, Config, rDictionary){
    console.log('mainCtrl is ready!');
    var that = this;
    window.scope = $scope;
    $scope.app = app;
    $scope.Storage = Storage;
    $scope.imagePath = 'i/washedout.png';

    this.init = function(){


        blocksList.init(function(){
            that.blocks = blocksList.items;
        });

        rDictionary.get(1, function(data){
            that.fLoad = false;
            Storage.fonts = data.items[0].value;
            console.info('rDictionary.query - fonts', data, Storage.fonts);
        });
    };



    this.toggleSidenav = function(menuId) {
        $mdSidenav(menuId).toggle();
    };

    app.onApply = function(){
        $scope.$apply();
    };

    app.screen.load(app.pages.home);


    this.goToBlock = function(block){
        Storage.block = block;
        if(app.screen.current.name != 'block'){
            that.goToPage('block', true, function(){
                //Storage.initBlock();
            });
        }else{
            Storage.initBlock();
        }
    };


    this.goToPage = function(pageName, closeNav, f){
        app.screen.load(app.pages[pageName], f);
        if( closeNav ){
            $mdSidenav('left').close();
        }
    };

    this.goToLink = function(link){
        console.log(link);
        location.replace(link);
    };

    this.checkRoot = function(){
        if(Config.meta['_user_role']==0){
            Storage.isRoot = true;
        }
    };

    that.init();

    console.log('mainCtrl is end!');

});
/**
 * Created by emaxe on 10.07.2015.
 */
app.controller('LeftCtrl', function($scope, $timeout, $mdSidenav, $log){
    console.log('leftCtrl is ready!');
    var that = this;
    $scope.app = app;

    //$scope.close = function () {
    //    $mdSidenav('left').close()
    //        .then(function () {
    //            $log.debug("close LEFT is done");
    //        });
    //};

    console.log('leftCtrl is end!');
});
app.controller('DialogCtrl', function($scope, $mdDialog, Storage, InScope){
    console.log('dialogCtrl is ready!');
    var that = this;
    $scope.app = app;
    $scope.Storage = Storage;
    $scope.InScope = InScope;

    Storage.dialogSubHeader = '';

    $scope.cancel = function() {
        $mdDialog.cancel();
    };

});
/**
 * Created by emaxe on 07.10.2015.
 */


app.factory('blocksList', function(rContent){
    var that = this;

    this.fLoad = false;

    this.items = [];

    this.init = function (f){
        that.fLoad = true;
        rContent.query({key: 'group', value: 'blocks-items'}, function(data){
            that.fLoad = false;
            for(var i=0;i<data.items.length;i++){
                that.items.push(data.items[i].value);
            }
            //that.items = data.items;
            if(typeof(f)=='function'){
                f(data);
            }
            console.info('rContent.query - blocks', data);
        });
    };



    return this;
});
/**
 * Created by emaxe on 13.07.2015.
 */

app.factory('Config',function(){
    var that = this;

    this.meta = {};

    this.meta.token = $('meta[name=_token]').attr('content');
    this.meta.host = $('meta[name=_app_host]').attr('content');
    this.meta.userRole = $('meta[name=_user_role]').attr('content');


    var metas = document.getElementsByTagName('meta');
    for (var i=0; i<metas.length; i++) {
        var name = metas[i].getAttribute("name");
        if(name!=null){
            that.meta[name] = metas[i].getAttribute("content");
        }
    }

    this.mainUrl = 'http://admin.'+this.meta.host;
    this.publicUrl = 'http://'+this.meta.host;

    return this;
});
/**
 * Created by emaxe on 13.07.2015.
 */

app.factory('rContent',['$resource', 'Config', function($resource, Config){
    return $resource('apiContent/:id', null, {
        update: {
            method: 'POST',
            url: 'apiContentUpdate',
            isArray: false
        },
        query: {
            method: 'GET',
            isArray: false
        },
        save: {
            method:'POST',
            url: 'apiContentSave',
            isArray: false
        },
        save2: {
            method:'POST',
            url: 'apiContentSave2',
            isArray: false
        },
        create: {
            method:'POST',
            url: 'apiContentCreate',
            isArray: false
        },
        create2: {
            method:'POST',
            url: 'apiContentCreate2',
            isArray: false
        },
        delete: {
            method:'POST',
            url: 'apiContentDelete',
            isArray: false
        }
    });
}]);

app.factory('rContentField',['$resource', 'Config', function($resource, Config){
    return $resource('apiContentField/:id', null, {
        update: {
            method: 'PUT'
        },
        query: {
            method: 'GET',
            isArray: false
        },
        save: {
            method:'POST',
            url: 'apiContentFieldSave',
            isArray: false
        },
        create: {
            method:'POST',
            url: 'apiContentFieldCreate',
            isArray: false
        },
        delete: {
            method:'POST',
            url: 'apiContentFieldDelete',
            isArray: false
        }
    });
}]);

app.factory('rContentType',['$resource', 'Config', function($resource, Config){
    return $resource('apiContentType/', null, {
        update: {
            method: 'PUT'
        },
        query: {
            method: 'GET',
            isArray: false
        },
        save: {
            method:'POST',
            url: 'apiContentTypeSave',
            isArray: false
        },
        create: {
            method:'POST',
            url: 'apiContentTypeCreate',
            isArray: false
        },
        delete: {
            method:'POST',
            url: 'apiContentTypeDelete',
            isArray: false
        }
    });
}]);

app.factory('rGallery',['$resource', 'Config', function($resource, Config){
    return $resource('apiGallery/:id', null, {
        update: {
            method: 'POST',
            url: 'apiGalleryUpdate',
            isArray: false
        },
        query: {
            method: 'GET',
            isArray: false
        },
        save: {
            method:'POST',
            url: 'apiGallerySave',
            isArray: false
        },
        create: {
            method:'POST',
            url: 'apiGalleryCreate',
            isArray: false
        },
        createImg: {
            method:'POST',
            url: 'apiGalleryCreateImg',
            isArray: false
        },
        delete: {
            method:'POST',
            url: 'apiGalleryDelete',
            isArray: false
        },
        deleteImg: {
            method:'POST',
            url: 'apiGalleryDeleteImg',
            isArray: false
        }
    });
}]);

app.factory('rBlocks',['$resource', 'Config', function($resource, Config){
    return $resource('apiBlocks/:id', null, {
        update: {
            method: 'POST',
            url: 'apiBlocksUpdate',
            isArray: false
        },
        query: {
            method: 'GET',
            isArray: false
        },
        save: {
            method:'POST',
            url: 'apiBlocksSave',
            isArray: false
        },
    });
}]);

app.factory('rDictionary',['$resource', 'Config', function($resource, Config){
    return $resource('apiDictionary/:id', null, {
        query: {
            method: 'GET',
            isArray: false
        },
    });
}]);

app.factory('rSms',['$resource', 'Config', function($resource, Config){
    return $resource('apiSms/:id', null, {
        query: {
            method: 'GET',
            isArray: false
        },
        send: {
            method:'POST',
            url: 'apiSmsSend',
            isArray: false
        },
        info: {
            method:'POST',
            url: 'apiSmsInfo',
            isArray: false
        }
    });
}]);
/**
 * Created by emaxe on 10.07.2015.
 */

//if(app==undefined){
//    var app = angular.module('app', []);
//}

app.factory('Storage', function(){
    var that = this;

    this.subHeader = '';
    this.topMenuItems = null;
    this.headerTabActive = null;

    this.fLoad = false;

    this.jsLoadArray = [];


    this.meta = null;
    this.menu = null;

    this.initBlock = function(){};

    this.galleries = [];

    this.isRoot = false;


    this.fieldTypes = {
        string: {
            name: 'string',
            title:'Строка'
        },
        text: {
            name: 'text',
            title:'Текст'
        },
        int: {
            name: 'int',
            title:'Число'
        },
        group: {
            name: 'group',
            title:'Группа'
        },
        img: {
            name: 'img',
            title:'Картинка'
        },
        boolean: {
            name: 'boolean',
            title:'Логическое значение'
        },
        gallery: {
            name: 'gallery',
            title:'Галлерея'
        },
        html: {
            name: 'html',
            title:'HTML'
        },
        phone: {
            name: 'phone',
            title:'Телефон'
        },
        email: {
            name: 'email',
            title:'E-mail'
        },
        font:{
            name: 'font',
            title: 'Шрифт'
        }
    };

    return this;
});
/**
 * Created by emaxe on 13.07.2015.
 */

app.directive('menuLine', function(Storage){
    return {
        templateUrl: 'ng/directives/menuLine.html',
        restrict: 'E',
        scope: {
            items: '='
        },
        link: function(scope, e, attrs){
            scope.Storage = Storage;
            console.log('DML => ', scope.items);

            //scope.$watch(Storage, function(){
            //    scope.Storage = Storage;
            //})
        }
    }
});
/**
 * Created by emaxe on 14.07.2015.
 */
app.factory('helper', function($mdToast, $mdDialog, app, Storage){
    var that = this;

    this.showToast = function(text){
        $mdToast.show(
            $mdToast.simple()
                .content(text)
                .position('bottom right')
                .hideDelay(3000)
        );
    };

    this.isArrayTrue = function(arr){
        var f = true;
        for(var i=0;i<arr.length;i++){
            if(arr[i]!=true){
                f = false;
                break;
            }
        }
        return f;
    };

    this.isObjectTrue = function(arr){
        var f = true;
        for(var key in arr){
            if(arr[key]!=true){
                f = false;
                break;
            }
        }
        return f;
    };

    this.setArray = function(arr, val){
        for(var i=0;i<arr.length;i++){
            arr[i] = val;
        }
        return arr;
    };

    this.setObject = function(arr, val){
        for(var key in arr){
            arr[key] = val
        }
        return arr;
    };

    this.concatObjects = function(o1,o2,replace){
        if(replace==undefined) replace = true;
        for(var i in o2){
            if(o1[i]==undefined || (o1[i]!=undefined && replace)){
                o1[i] = o2[i];
            }
        }
    };

    this.translit = function(word){

        var answer = "";
        var a = {}

        a["Ё"]="YO";a["Й"]="I";a["Ц"]="TS";a["У"]="U";a["К"]="K";a["Е"]="E";a["Н"]="N";a["Г"]="G";a["Ш"]="SH";a["Щ"]="SCH";a["З"]="Z";a["Х"]="H";a["Ъ"]="'";
        a["ё"]="yo";a["й"]="i";a["ц"]="ts";a["у"]="u";a["к"]="k";a["е"]="e";a["н"]="n";a["г"]="g";a["ш"]="sh";a["щ"]="sch";a["з"]="z";a["х"]="h";a["ъ"]="'";
        a["Ф"]="F";a["Ы"]="I";a["В"]="V";a["А"]="a";a["П"]="P";a["Р"]="R";a["О"]="O";a["Л"]="L";a["Д"]="D";a["Ж"]="ZH";a["Э"]="E";
        a["ф"]="f";a["ы"]="i";a["в"]="v";a["а"]="a";a["п"]="p";a["р"]="r";a["о"]="o";a["л"]="l";a["д"]="d";a["ж"]="zh";a["э"]="e";
        a["Я"]="Ya";a["Ч"]="CH";a["С"]="S";a["М"]="M";a["И"]="I";a["Т"]="T";a["Ь"]="'";a["Б"]="B";a["Ю"]="YU";
        a["я"]="ya";a["ч"]="ch";a["с"]="s";a["м"]="m";a["и"]="i";a["т"]="t";a["ь"]="'";a["б"]="b";a["ю"]="yu";

        for (i = 0; i < word.length; ++i){

            answer += a[word[i]] === undefined ? word[i] : a[word[i]];
        }
        return answer;
    };

    this.confirm = function(o){
        if(o.yes==undefined){
            o.yes = {};
        }
        if(o.no==undefined){
            o.no = {};
        }
        if (o.title==undefined){
            o.title = 'Подтвердите действие';
        }
        if (o.text==undefined){
            o.text = 'Точно?';
        }
        if (o.yes.title==undefined){
            o.yes.title = 'Да';
        }
        if (o.no.title==undefined){
            o.no.title = 'Нет';
        }
        if (o.event==undefined){
            o.event = angular.element(document.body);
        }

        if (o.yes.onClick==undefined || typeof(o.yes.onClick)!='function'){
            o.yes.onClick = function(event){
                console.log('Yes!!');
            };
        }

        if (o.no.onClick==undefined || typeof(o.no.onClick)!='function'){
            o.no.onClick = function(event){
                console.log('Noooo!!');
            };
        }

        var confirm = $mdDialog.confirm()
            .parent(angular.element(document.body))
            .title(o.title)
            .content(o.text)
            .ariaLabel(o.title)
            .ok(o.yes.title)
            .cancel(o.no.title)
            .targetEvent(o.event)
            .clickOutsideToClose(true);
        $mdDialog.show(confirm).then(function() {
            o.yes.onClick();
        }, function() {
            o.no.onClick();
        });


    };

    this.showDialog = function(o){
        if(o.ev == undefined){
            o.ev = angular.element(document.body);
        }
        app.getJS('ng/admin/dialogs/'+ o.controllerName+'Ctrl', function(){
            $mdDialog.show({
                controller: 'DialogCtrl',
                clickOutsideToClose: true,
                templateUrl: 'ng/dialogs/'+ o.tmpName+'.html',
                parent: angular.element(document.body),
                targetEvent: o.ev,
                locals: {
                    InScope: o.inScope
                }
            })
                .then(function(data) {
                    if(data.state=='ok'){
                        //that.fLoad = true;
                        //that.init();
                        if(typeof(o.success)=='function'){
                            o.success(data);
                        }
                    }else{
                        if(typeof(o.error)=='function'){
                            o.error(data);
                        }
                    }
                }, function() {
                    if(typeof(o.cancel)=='function'){
                        o.cancel();
                    }
                });
        });
    };


    return this;
});


app.factory('InScope', function(){
    return this;
});
/**
 * Created by emaxe on 19.09.2015.
 */

app.directive('content', function($compile, RecursionHelper, helper, rContent, Storage){
    return {
        templateUrl: 'ng/directives/content.html',
        resctrict: 'E',
        scope: {
            item: '=',
            index: '=',
            editable: '=',
            fLoad: '=',
            reload: '=',
            change: '=?'
        },
        compile: function(element){
            return RecursionHelper.compile(element, function(scope, e, attrs, controller, transcludeFn){
                scope.Storage = Storage;
                scope.tinymceOptions = {
                    handle_event_callback: function (e) {
                        // put logic here for keypress
                    },
                    theme: "modern",
                    height : 375,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
                    ],
                    schema: "html5",
                    toolbar: "undo redo | fontselect fontsizeselect styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image forecolor backcolor tablecontrols    | print preview media | responsivefilemanager | code",
                    statusbar: false,
                    language : 'ru',
                    image_advtab: true ,

                    relative_urls: false,
                    external_filemanager_path:"/tinymce/filemanager/",
                    filemanager_title:"Менеджер файлов" ,
                    external_plugins: { "filemanager" : "filemanager/plugin.min.js"}
                };

                $('.iframe-btn').fancybox({
                    'width'		: 900,
                    'height'	: 600,
                    'type'		: 'iframe',
                    'autoScale'    	: false
                });

                setTimeout(function(){
                    $('.iframe-btn').fancybox({
                        'width'		: 900,
                        'height'	: 600,
                        'type'		: 'iframe',
                        'autoScale'    	: false
                    });
                }, 200);

                setTimeout(function(){
                    $('.iframe-btn').fancybox({
                        'width'		: 900,
                        'height'	: 600,
                        'type'		: 'iframe',
                        'autoScale'    	: false
                    });
                }, 1000);

                scope.RFMInit = function(item){

                    console.info("F");
                    $('.iframe-btn').fancybox({
                        'width'		: 900,
                        'height'	: 600,
                        'type'		: 'iframe',
                        'autoScale'    	: false
                    });

                    setTimeout(function(){
                        console.info("F");
                        $('.iframe-btn').fancybox({
                            'width'		: 900,
                            'height'	: 600,
                            'type'		: 'iframe',
                            'autoScale'    	: false
                        });
                    }, 1000);

                    console.info('RFMInit - ', item, scope.item);
                    var f = function(field_id){
                        var o = document.getElementById(field_id);
                        var url = o.value;
                        for(var i=0; i<scope.item.fields.length;i++){
                            var field = scope.item.fields[i];
                            if(field_id == 'imgField_'+scope.item.id+'_'+field.id){
                                scope.item.value[field.name] = url;
                                console.info('Catch!!! ', field_id, o.value, field);
                                scope.$apply();
                                break;

                            }
                        }
                        console.log('Catch! ', field_id, o.value, scope.item);
                    };

                    for(var i=0; i<scope.item.fields.length;i++){
                        var field = scope.item.fields[i];

                        var value = scope.item.value[field.name];

                        if(value==null){
                            console.log('Value is NULL! ', field, value, field.default);
                            scope.item.value[field.name] = field.default;
                        }

                        if(field.type=='select'){
                            field.select = JSON.parse(field.select);
                        }

                        if(field.type=='animation'){
                            field.select = JSON.parse(field.select);
                            if(scope.item.value[field.name].delay==undefined) scope.item.value[field.name].delay=0;
                            if(scope.item.value[field.name].duration==undefined) scope.item.value[field.name].duration=0;
                        }

                        if(field.type=='img'){

                            var ff = true;
                            RFMCallbacks.map(function(o){
                                if(o.id==scope.item.id+'_'+field.id){
                                    o.go = f;
                                    ff = false;
                                }
                            });

                            if(ff){
                                console.log('--> ADDED ', ff, scope.item.id+'_'+field.id);
                                RFMCallbacks.push({
                                    id: scope.item.id+'_'+field.id,
                                    go: f
                                });
                            }

                        }
                    }
                };

                scope.addContent = function(ev, field){

                    helper.confirm({
                        title: 'Создание нового элемента',
                        text: 'Точно хотите создать новый элемент?',
                        yes: {
                            title: 'Создать',
                            onClick: function(){
                                scope.fLoad = true;
                                rContent.create({field:field, item_id: scope.item.id, type:'group'}, function(data){
                                    console.info('rContent.create - items', data);
                                    scope.fLoad = false;
                                    helper.showToast('Элемент создан!');
                                    scope.reload();
                                });
                            }
                        },
                        no: {
                            title: 'Отмена'
                        },
                        event: ev
                    });
                };

                scope.deleteContent = function(ev, item){

                    helper.confirm({
                        title: 'Удаление элемента',
                        text: 'Точно хотите удалить элемент?',
                        yes: {
                            title: 'Удалить',
                            onClick: function(){
                                scope.fLoad = true;
                                rContent.delete({id: item.id}, function(data){
                                    console.info('rContent.delete - items', data);
                                    scope.fLoad = false;
                                    helper.showToast('Элемент удален!');
                                    scope.reload();
                                });
                            }
                        },
                        no: {
                            title: 'Отмена'
                        },
                        event: ev
                    });
                };

                var checkItem = function(){
                    console.info('if(angular.isObject(scope.item)) -> ', scope.item, angular.isObject(scope.item));
                    if(angular.isObject(scope.item)){
                        scope.RFMInit();
                    }
                };

                checkItem();

                scope.$watch('change', function(){
                    checkItem();
                    scope.change = false;
                });

            });
        }
    }
});
/**
 * Created by emaxe on 19.09.2015.
 */

app.directive('myImg', function(){
    return {
        templateUrl: 'ng/directives/myImg.html',
        restrict: 'E',
        scope: {
            img: '=',
            alt: '='
        },
        link: function(scope, e, attrs){
            console.log('selectGallery => ', scope.img);
        }
    }
});
/**
 * Created by emaxe on 19.09.2015.
 */

app.directive('selectGallery', function(Storage, rGallery){
    return {
        templateUrl: 'ng/directives/selectGallery.html',
        restrict: 'E',
        scope: {
            model: '='
        },
        link: function(scope, e, attrs){
            scope.Storage = Storage;
            console.log('selectGallery => ', scope.model);

            scope.items = Storage.galleries;

            rGallery.query(function(data){
                console.log('rGallery.query => ', data);
                scope.items = data.items;
                Storage.galleries = data.items;
            });
        }
    }
});
//# sourceMappingURL=ngApp.js.map
