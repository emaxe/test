<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




$adminDomain = function(){
    Route::get('/', 'AdminController@index');

    Route::resource('apiContent', 'ApiContentController');
    Route::post('apiContentSave', ['as' => 'ApiContentSave', 'uses' => 'ApiContentController@save']);
    Route::post('apiContentSave2', ['as' => 'ApiContentSave2', 'uses' => 'ApiContentController@save2']);
    Route::post('apiContentCreate', ['as' => 'ApiContentCreate', 'uses' => 'ApiContentController@contentCreate']);
    Route::post('apiContentCreate2', ['as' => 'ApiContentCreate2', 'uses' => 'ApiContentController@contentCreate2']);
    Route::post('apiContentDelete', ['as' => 'ApiContentDelete', 'uses' => 'ApiContentController@delete']);
    Route::post('apiContentUpdate', ['as' => 'ApiContentUpdate', 'uses' => 'ApiContentController@update']);

    Route::resource('apiGallery', 'ApiGalleryController');
    Route::post('apiGalleryUpdate', ['as' => 'ApiGalleryUpdate', 'uses' => 'ApiGalleryController@updateModel']);
    Route::post('apiGallerySave', ['as' => 'ApiGallerySave', 'uses' => 'ApiGalleryController@save']);
    Route::post('apiGalleryCreate', ['as' => 'ApiGalleryCreate', 'uses' => 'ApiGalleryController@galleryCreate']);
    Route::post('apiGalleryCreateImg', ['as' => 'ApiGalleryCreateImg', 'uses' => 'ApiGalleryController@galleryCreateImg']);
    Route::post('apiGalleryDelete', ['as' => 'ApiGalleryDelete', 'uses' => 'ApiGalleryController@delete']);
    Route::post('apiGalleryDeleteImg', ['as' => 'ApiGalleryDeleteImg', 'uses' => 'ApiGalleryController@deleteImg']);

    Route::resource('apiBlocks', 'ApiBlocksController');
    Route::post('apiBlocksUpdate', ['as' => 'ApiBlocksUpdate', 'uses' => 'ApiBlocksController@updateModel']);
    Route::post('apiBlocksSave', ['as' => 'ApiBlocksSave', 'uses' => 'ApiBlocksController@save']);

    Route::resource('apiDictionary', 'ApiDictionaryController');

    Route::resource('apiContentField', 'ApiContentFieldController');
    Route::post('apiContentFieldSave', ['as' => 'ApiContentFieldSave', 'uses' => 'ApiContentFieldController@save']);
    Route::post('apiContentFieldCreate', ['as' => 'ApiContentFieldCreate', 'uses' => 'ApiContentFieldController@contentFieldCreate']);
    Route::post('apiContentFieldDelete', ['as' => 'ApiContentFieldDelete', 'uses' => 'ApiContentFieldController@delete']);

    Route::resource('apiContentType', 'ApiContentTypeController');
    Route::post('apiContentTypeSave', ['as' => 'ApiContentTypeSave', 'uses' => 'ApiContentTypeController@save']);
    Route::post('apiContentTypeCreate', ['as' => 'ApiContentTypeCreate', 'uses' => 'ApiContentTypeController@contentTypeCreate']);
    Route::post('apiContentTypeDelete', ['as' => 'ApiContentTypeDelete', 'uses' => 'ApiContentTypeController@delete']);

    Route::get('apiSms', 'ApiSmsController@index');
    Route::get('apiSmsInfo', 'ApiSmsController@info');
    Route::post('apiSmsSend', 'ApiSmsController@send');
};

Route::group(['middleware' => ['web'], 'domain' => 'admin.'.env('APP_HOST', 'ss-legion.dev')], function() use($adminDomain)
//Route::group(['middleware' => ['web'], 'domain' => 'admin.'.env('APP_HOST', 'lika-rnd.ru')], function() use($adminDomain)
{
    $adminDomain();
});

Route::group(['middleware' => ['web'], 'prefix' => 'admin'], function() use($adminDomain)
{
    $adminDomain();
});

Route::group(['middleware' => ['web']], function() use($adminDomain)
{
    $adminDomain();
});

Route::resource('publicApiContent', 'PublicApiContentController');
Route::post('publicApiAddToast', ['as' => 'publicApiAddToast', 'uses' => 'PublicApiContentController@addToast']);

Route::resource('publicApiGallery', 'PublicApiGalleryController');
Route::post('publicFileUpload/upload', 'PublicFileUploadController@upload');

Route::post('publicApiSmsSend', 'PublicApiSmsController@send');

Route::get('admin', 'AdminController@index');

//Route::get('/', ['as' => 'welcome', 'middleware' => 'auth', 'uses' => 'WelcomeController@adminIndex']);
Route::get('/', ['as' => 'welcome', 'uses' => 'WelcomeController@index']);









//
//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index');
