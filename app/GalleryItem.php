<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryItem extends Model {

    protected $fillable = [
        'id',
        'gallery_id',
        'title',
        'description',
        'img',
        'order',
        'published'
    ];

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'asc');
    }

    public function scopeName($query, $name)
    {
        $query->where('name', '=', $name);
    }

    //=======================================================

    public function gallery(){
        $types = $this->belongsTo('App\Gallery', 'gallery_id')->get()[0];
        return $types;
    }

}
