<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Content extends Model {

    protected $fillable = [
        'id',
        'pid',
        'type_id',
        'name',
        'title',
        'group',
        'parent_group',
        'order',
        'published',
        'value',
    ];

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'asc');
    }

    public function scopeName($query, $name)
    {
        $query->where('name', '=', $name);
    }

    public function scopeGroup($query, $groupName)
    {
        $query->where('group', '=', $groupName);
    }

    public function scopeParentGroup($query, $groupName)
    {
        $query->where('parent_group', '=', $groupName);
    }

    public function scopeFindInGroup($query, $groupName, $id)
    {
        $query->where('group', '=', $groupName)->where('id', '=', $id);
    }

    public function scopePublished($query)
    {
        $query->where('published', '=', true);
    }

    public function scopeUnublished($query)
    {
        $query->where('published', '=', false);
    }

    //==========================================================

    public function value($all=true)
    {
        if($this->value!=null){
            $o = json_decode($this->value);
        }else{
            $o = $this;
        }
        return $o;
    }

    public function valueFull($content=null){
        // ToDo this shit...
    }

    public function isGroup(){
        $f = false;
        $fields = $this->fields();
        foreach($fields as $field){
            if($field->type == 'group'){
                $f = true;
                break;
            }
        }
        return $f;
    }

    public function getGroupName(){
        $f = null;
        $fields = $this->fields();
        foreach($fields as $field){
            if($field->type == 'group'){
                $value = (array) $this->value();
                if($value!=null){
                    $f = $value[$field->name];
                }
                break;
            }
        }
        return $f;
    }

    public function getGroupsNames(){
        $arr = [];
        $fields = $this->fields();
        foreach($fields as $field){
            if($field->type == 'group'){
                $value = (array) $this->value();
                if($value!=null){
                    $arr[] = $value[$field->name];
                }
            }
        }
        return $arr;
    }

    public function getGroupsItems($published=true)
    {
        $r_arr = [];
        $groupNames = $this->getGroupsNames();
        foreach ($groupNames as $groupName){
            if($published){
                $group = $group = Content::group($groupName)->published()->ordered()->get();
            }else{
                $group = $group = Content::group($groupName)->ordered()->get();
            }
            $arr = [];
            foreach($group as $g){
                $g->value = $g->value();
                $arr[] = $g;
            }
            if(!isset($this->gf)){
                $this->gf = [];
            }
            $name = $groupName;
            $r_arr[$name] = $arr;
//            $this->gf[] = $arr;
        }
        return $r_arr;
    }

    public function getGroupItems($published=true)
    {
        $groupName = $this->getGroupName();
        if($groupName!=null){
            if($published){
                $group = $group = Content::group($groupName)->published()->ordered()->get();
            }else{
                $group = $group = Content::group($groupName)->ordered()->get();
            }
            $arr = [];
            foreach($group as $g){
                $g->value = $g->value();
                $arr[] = $g;
            }
            return $arr;
        }else{
            return null;
        }
    }
    //==========================================================

    public function type(){
        Log::info('CONTENT -> TYPE() :=> '.$this->type_id);
        $type = $this->belongsTo('App\ContentType', 'type_id')->get()[0];
        return $type;
    }

    public function fields(){
        return $fields = $this->type()->fields();
    }

    public function tags(){
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

}
