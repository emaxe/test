<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model {

    protected $fillable = [
        'id',
        'name',
        'title',
        'order',
        'published'
    ];

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'asc');
    }

    public function scopeName($query, $name)
    {
        $query->where('name', '=', $name);
    }

    //==================================================

    public function items()
    {
        return $this->hasMany('App\GalleryItem', 'gallery_id')->orderBy('order')->get();
    }

}
