<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model {

    protected $fillable = [
        'id',
        'title',
        'name',
        'description'
    ];

    //============================================

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'asc');
    }

    public function scopeName($query, $name)
    {
        $query->where('name', '=', $name);
    }

    //============================================

    public function contents()
    {
        return $this->hasMany('App\Content', 'type_id')->get();
    }

    public function fields()
    {
        return $this->hasMany('App\ContentField', 'content_type_id')->orderBy('order')->get();
    }

}
