<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        $isNg = false;

        if(isset($request->header()->app) && $request->header()->app[0]=='ng'){
            $isNg = true;
        }

        if($this->auth->user()!=null){
            $role = $this->auth->user()->toArray()['role'];
        }else{
            $role = null;
        }
        if ($this->auth->check() && ($role==1 || $role==0)){
            return $next($request);
        }else{
            if ($this->auth->guest())
            {
                if ($request->ajax() || $isNg)
                {
//                    return response('Unauthorized.', 401);
                    return response(json_encode(['status'=>'unautorized']), 401);
                }
                else
                {
                    return redirect()->guest('login');
                }
            }else{
                if ($request->ajax() || $isNg)
                {
//                    return response('Access denied.', 401);
                    return response(json_encode(['status'=>'access denied']), 401);
                }
                else
                {
                    return view('errors.accessDenied');
                }
            }
        }
    }
}
