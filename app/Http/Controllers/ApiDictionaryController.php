<?php

namespace App\Http\Controllers;

use App\Dictionary;
use Illuminate\Http\Request;

use App\Http\Requests;
use Response;

class ApiDictionaryController extends Controller
{
    public function index(Request $request)
    {
        $items = Dictionary::ordered()->get();

        $arr = [];
        foreach($items as $item){
            $item->value = $item->value();
            $arr[] = $item;
        }
        $items = $arr;

        return Response::json([
            'items' => $items,
            'action' => 'index',
            'model' => 'dictionary',
            'request' => $request->all()
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $item = Dictionary::findOrFail($id);
        $item->value = $item->value();
        return Response::json([
            'item' => $item,
            'action' => 'show',
            'model' => 'dictionary'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function updateModel(Request $request)
    {
        $log = [];

        return Response::json([
            'request' => $request->all(),
            'action' => 'update',
            'model' => 'dictionary',
            'log' => $log
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {

        return Response::json([
            'action' => 'delete',
            'model' => 'dictionary',
            'request' => $request->all(),
        ]);
    }
}
