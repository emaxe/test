<?php namespace App\Http\Controllers;

use App\Content;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\Sms;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Response;

class ApiSmsController extends Controller {

	public function __construct(Sms $sms){
		$this->middleware('isAdmin');

		$this->sms = $sms;
	}

	private $sms;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$info['balance'] = $this->sms->balance();
		$info['api_version'] = $this->sms->api_version();
		$info['senders'] = $this->sms->senders();

//		$res = $this->sms->send('+79185680868', 'PREVED!');

		return Response::json([
			'action' => 'index',
			'model' => 'sms',
			'info' => $info
		]);
	}

	/**
	 * @return \Illuminate\View\View
     */
	public function info()
	{
		$log = [];
		$info = [];

		$info['balance'] = $this->sms->balance();
		$info['api_version'] = $this->sms->api_version();
		$info['senders'] = $this->sms->senders();
		return Response::json([
			'action' => 'contentCreate',
			'model' => 'sms',
			'info' => $info,
			'log' => $log
		]);
	}

	public function send(Request $request)
	{

		$v = Validator::make($request->all(), [
			'phone' => 'required'
		],[
			'phone.required' => 'Телефон обязателен для заполнения!'
		]);
		if ($v->fails())
		{
			return $v->errors();
		}

		$this->validate($request,[
			'phone' => 'required'
		],[
			'phone.required' => 'Телефон обязателен для заполнения!'
		]);

		$meta = (object) Content::name("meta")->first()->value();

		$req = (array) $request->all();
		$msg = "Запрос sochi-legion.ru\nИмя: ".$request->get('name')." ;\r\n Телефон:".$request->get('phone')." ;\n Текст:".$request->get('text').';';
		$res = '-';
		if ($meta->smsSend){
			$res = $this->sms->send($meta->smsPhone, $msg);
		}


		return Response::json([
			'res' => $res,
			'msg' => $msg,
			'req' => $req,
			'meta' => $meta,
			'action' => 'send',
			'model' => 'apiSms',
			'request' => $request->all()
		]);
	}

}
