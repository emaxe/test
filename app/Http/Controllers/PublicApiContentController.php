<?php namespace App\Http\Controllers;

use App\Content;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Response;

class PublicApiContentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		if($request->has('key') && $request->has('value')){
			$relationship = '=';
			if($request->has('relationship')) $relationship = $request->get('relationship');
			$items = Content::published()->where($request->get('key'), $relationship, $request->get('value'))->ordered()->get();
		}else{
			$items = Content::ordered()->get();
		}

		$items = $this->prepareItems($items);

		return Response::json([
			'items' => $items,
			'action' => 'index',
			'model' => 'content',
			'request' => $request
		]);
	}

	private function prepareItems($items){
		foreach($items as $item){
			$item->type = $item->type();
			$item->fields = $item->type->fields();
			$item->value = $item->value();



			foreach($item->fields as $field){
				switch($field->type){
					case 'select':
						if($field->select_id>0){
							$field->select = $field->select()->value();
						}
						break;
					case 'group':
						$value = (array)$item->value;
						$group = $value[$field->name];
						$nItems = Content::group($group)->ordered()->get();
						$value[$field->name] = $this->prepareItems($nItems);
						$item->value = (object) $value;
						break;
					default:
						//
						break;
				}
			}
		}

		return $items;
	}

    public function addToast(Request $request)
    {
        $log = [];
        if($request->has('item')){
            $item = $request->get('item');
			if(!isset($item['name'])) $item['name'] = '(none)';
			if(!isset($item['phone'])) $item['phone'] = '(none)';
			if(!isset($item['email'])) $item['email'] = '(none)';
			if(!isset($item['contacts'])) $item['contacts'] = '(none)';
			if(!isset($item['text'])) $item['text'] = '(none)';
			if(!isset($item['file'])) $item['file'] = '(none)';
            $val = [
                "is_new" => true,
                "name" => $item['name'],
                "phone" => $item['phone'],
                "email" => $item['email'],
                "contacts" => $item['contacts'],
                "text" => $item['text'],
                "file" => $item['file'],
            ];

            $val = (object)$val;
            $item = Content::create([
                "type_id" => '50',
                "group" => 'order',
                "value" => json_encode($val)
            ]);

			$meta = Content::name("meta")->first()->value();

			if($meta->emailSend){

				Mail::send('emails.order', compact('item'), function ($message) use($meta) {
					$message
						->to($meta->emailAdress)
						->from('admin@kvalitet-ug.ru')
						->subject('Запрос с сайта kvalitet-ug.ru');
				});
			}

        }
        return Response::json([
            'action' => 'addToast',
            'item' => $item,
            'model' => 'public - content',
            'log' => $log,
            'request' => $request->all()
        ]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
