<?php namespace App\Http\Controllers;

use App\Content;
use App\ContentField;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Response;

class ApiContentFieldController extends Controller {

	public function __construct(){
		\Barryvdh\Debugbar\Facade::disable();
		$this->middleware('isAdmin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function index(Request $request)
	{
		if($request->has('key') && $request->has('value')){
			$relationship = '=';
			if($request->has('relationship')) $relationship = $request->get('relationship');
			$items = ContentField::where($request->get('key'), $relationship, $request->get('value'))->ordered()->get();
		}else{
			$items = ContentField::ordered()->get();
		}

//		$items = $this->prepareItems($items);

		if($request->has('prepare') && $request->get('prepare')==true){
			$items = $this->prepareItems($items);
		}

		return Response::json([
			'items' => $items,
			'action' => 'index',
			'model' => 'contentField',
			'request' => $request->all()
		]);
	}

	private function prepareItems($items){
		foreach($items as $item){
			$item->Type = $item->type();
			$item->Select = $item->select();
		}

		return $items;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	public function contentFieldCreate(Request $request)
	{
		$log = [];

		ContentField::create($request->all());

		return Response::json([
			'action' => 'contentFieldCreate',
			'model' => 'contentField',
			'log' => $log,
			'request' => $request->all()
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	public function save(Request $request)
	{
		$log = [];
		$id = $request->get('id');
		$item = ContentField::find($id);

		/** @var TYPE_NAME $item */
		$item->update($request->all());

		$item = ContentField::find($id);

		if($item->type=='group'){
			$type = $item->type();
			$contents = Content::where('type_id','=',$type->id)->get();
			$log['type'] = $type;
			$log['contents'] = $contents;

			foreach($contents as $content){
				$content->value = $content->value();
				$log['content_'.$content->id] = $content;

				$f = false;
				foreach($content->value as $key => $value){
					if($key == $item->name){
						$f = true;
						$value = $request->get('group_name');
						break;
					}
				}
				if(!$f){
					$log[] = [
						'value' => (array)$content->value,
						'name' => $item->name,
						'group' => $request->get('group_name')
					];
					$content->value = array_add((array)$content->value, $item->name, $request->get('group_name'));
				}

				$content->value = json_encode($content->value);

				$content->save();
			}
		}

		return Response::json([
			'item' => $item,
			'request' => $request->all(),
			'action' => 'save',
			'model' => 'contentField',
			'log' => $log
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function delete(Request $request)
	{
		if($request->has('id')){
			ContentField::destroy($request->get('id'));
		}
		return Response::json([
			'action' => 'destroy',
			'model' => 'contentField',
			'request' => $request->all(),
		]);
	}

}
