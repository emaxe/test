<?php

namespace App\Http\Controllers;

use App\Content;
use Illuminate\Http\Request;
use Response;

class ApiBlocksController extends Controller
{
    public function index(Request $request)
    {
        $items = Content::where('type_id',5)->ordered()->get();

        return Response::json([
            'items' => $items,
            'action' => 'index',
            'model' => 'blocks',
            'request' => $request->all()
        ]);
    }

    public function updateModel(Request $request)
    {
        $log = [];
        $item = Content::find($request->get('id'));

        $item->update($request->get('item'));

        return Response::json([
            'item' => $item,
            'request' => $request->all(),
            'action' => 'update',
            'model' => 'blocks',
            'log' => $log
        ]);
    }
}
