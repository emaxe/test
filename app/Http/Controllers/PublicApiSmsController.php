<?php namespace App\Http\Controllers;

use App\Content;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Services\Sms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;

class PublicApiSmsController extends Controller {

	public function __construct(Sms $sms){
		$this->sms = $sms;
	}

	private $sms;

	public function send(Request $request)
	{

		$v = Validator::make($request->all(), [
			'contacts' => 'required'
		],[
			'contacts.required' => 'Телефон обязателен для заполнения!'
		]);
		if ($v->fails())
		{
			return $v->errors();
		}

		$this->validate($request,[
			'contacts' => 'required'
		],[
			'contacts.required' => 'Телефон обязателен для заполнения!'
		]);

		$meta = (object) Content::name("meta")->first()->value();

		$req = (array) $request->all();
//		$msg = "Запрос lika-rnd.ru\nИмя: ".$request->get('name')." ;\r\nКонтакты:".$request->get('contacts')." ;\n Текст:".$request->get('text').';';
		$msg = "Запрос sochi-legion.ru\nИмя: ".$request->get('name').";\r\nКонтакты: ".$request->get('phone').'/'.$request->get('email');
		$res = '-';
//		if ($meta->smsSend){
//			if($request->get('type')=='plugin'){
//				$res = $this->sms->send($meta->smsPhone, $msg, $meta->smsSender);
////				$phone = '+7'.substr($request->get('phone'),1);
//				$phone = $request->get('contacts');
//				$res = $this->sms->send($phone, "Номер вашего купона на скидку от zabor-rostov.ru :\r\n ".$request->get('code'), $meta->smsSender);
//			}else{
				$res = $this->sms->send($meta->smsPhone, $msg, $meta->smsSender);
//			}
//		}


		return Response::json([
			'res' => $res,
			'msg' => $msg,
			'req' => $req,
			'meta' => $meta,
			'action' => 'send',
			'model' => 'apiSms',
			'request' => $request->all()
		]);
	}

}
