<?php namespace App\Http\Controllers;

use App\Content;
use App\ContentField;
use App\Dictionary;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Log;
use Illuminate\Http\Request;
use Response;

class ApiContentController extends Controller {

	public function __construct(){
		\Barryvdh\Debugbar\Facade::disable();
		$this->middleware('isAdmin');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		Log::info('ApiContentController >> index >>');
		if($request->has('key') && $request->has('value')){
			$relationship = '=';
			if($request->has('relationship')) $relationship = $request->get('relationship');
			$items = Content::where($request->get('key'), $relationship, $request->get('value'))->ordered()->get();
		}else{
			$items = Content::ordered()->get();
		}

		$items = $this->prepareItems($items);

		return Response::json([
			'items' => $items,
			'action' => 'index',
			'model' => 'content',
			'request' => $request->all()
		]);
	}

	private function prepareItemsFields($fields){
		foreach($fields as $field){
			$field->Type = $field->type();
			$field->Select = $field->select();
		}

		return $fields;
	}

	private function prepareItems($items){
		foreach($items as $item){
			$item->type = $item->type();
			$item->fields = $item->type->fields();
//			$item->fields = $this->prepareItemsFields($item->fields);
			$item->value = $item->value();



			foreach($item->fields as $field){
				switch($field->type){
					case 'select':
						if($field->select_id>0){
							$select = $field->select();
							if($select!=null) {
								$field->select = $select->value();
							}else{
								$field->select = null;
							}
						}
						break;
					case 'animation':
						if($field->select_id>0){
							$select = $field->select();
							if($select!=null) {
								$field->select = $select->value();
							}else{
								$field->select = null;
							}
						}
						break;
					case 'group':
						$value = (array)$item->value;
						if(isset($value[$field->name])){
							$group = $value[$field->name];
							$nItems = Content::group($group)->ordered()->get();
							$value[$field->name] = $this->prepareItems($nItems);
							$item->value = (object) $value;
						}
						break;
					default:
						//
						break;
				}
			}
		}

		return $items;
	}

	public function contentCreate(Request $request)
	{
		$log = [];
		if($request->has('item_id') && $request->has('field') && $request->has('type')){
			$item_id = $request->get('item_id');
			$field = $request->get('field');
			$type = $request->get('type');

			if($type == 'group'){
				$item = Content::find($item_id);
				$itemType = $item->type();
				$itemFields = $itemType->fields();

				$value = (array) $item->value();
				$group = $value[$field['name']];

				$val = [];

				foreach($itemFields as $itemField){
					if($itemField->default!=null){
						$val[$itemField->name] = $itemField->default;
					}
				}

				$val = (object)$val;
				$val = json_encode($val);

				Content::create([
					"type_id" => $field['group_type_id'],
					"group" => $group,
					"value" => $val
				]);
			}

		}
		return Response::json([
			'action' => 'contentCreate',
			'model' => 'content',
			'log' => $log,
			'request' => $request->all()
		]);
	}

	public function contentCreate2(Request $request)
	{
		$log = [];

		Content::create($request->all());

		return Response::json([
			'action' => 'contentCreate',
			'model' => 'content',
			'log' => $log,
			'request' => $request->all()
		]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		return Response::json([
			'action' => 'create',
			'model' => 'content',
			'request' => $request->all()
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Response::json([
			'action' => 'store',
			'model' => 'content'
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$item = Content::findOrFail($id);
		$item->value = json_decode($item->value);
		return Response::json([
			'item' => $item,
			'action' => 'show',
			'model' => 'content'
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
	{
        $log = [];
		$id = $request->get('id');
        $item = Content::find($id);

		$all = $request->all();
		$all['value'] = json_encode($all['value']);

        /** @var TYPE_NAME $item */
        $item->update($request->all());

        $item = Content::find($id);

        return Response::json([
            'item' => $item,
            'request' => $request->all(),
            'action' => 'update',
            'model' => 'product',
            'all' => $all,
            'log' => $log
        ]);
	}

	public function save2(Request $request)
	{
		$log = [];

		$items = $this->saveRecursive($request->get('items'));

		return Response::json([
			'items' => $items,
			'request' => $request->all(),
			'action' => 'save',
			'model' => 'content',
			'log' => $log
		]);
	}

    public function save(Request $request)
    {
        $log = [];

		$items = $this->saveRecursive($request->all());

        return Response::json([
            'items' => $items,
            'request' => $request->all(),
            'action' => 'save',
            'model' => 'product',
            'log' => $log
        ]);
    }

	private function saveRecursive($items){
		foreach($items as $rItem){
			if(is_object($rItem)) $rItem = (array)$rItem;

			$item = Content::find($rItem['id']);
			$itemType = $item->type();
			$itemFields = $itemType->fields();
			$itemValue = (array) $item->value();

			if(is_string($rItem['value'])){
				$value = (array) json_decode($rItem['value']);
			}else{
				$value = ($rItem['value']);
				if(is_object($rItem['value'])) $rItem['value'] = (array)$rItem['value'];
			}

			foreach($itemFields as $field){

				if($field->type=='group'){
					$vItems = (array)$value[$field->name];
					$value[$field->name] = $itemValue[$field->name];

					$this->saveRecursive($vItems);

				}

			}

			$rItem['value'] = ($value);

			if(!is_string($rItem['value'])) $rItem['value'] = json_encode($rItem['value']);

			$item->update($rItem);

			$items[] = $item;
		}

		return $items;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Response::json([
			'id' => $id,
			'action' => 'destroy',
			'model' => 'product',
		]);
	}

	public function delete(Request $request)
	{
		if($request->has('id')){
			Content::destroy($request->get('id'));
		}
		return Response::json([
			'action' => 'destroy',
			'model' => 'product',
			'request' => $request->all(),
		]);
	}

}
