<?php namespace App\Http\Controllers;

use App\Content;
use \Barryvdh\Debugbar\Facade as Debug;
use Illuminate\Support\Facades\Mail;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */


	var $isAdmin = false;

	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
//		if(!isset($this->isAdmin)) $isAdmin=false;
		$stop = false;
		$meta = Content::name("meta")->first()->value();

		if(!$meta->siteStop){
			$blocks = Content::group("blocks-items")->published()->ordered()->get();

			$i = 0;
			$arr = [];
			foreach($blocks as $block){
				$block->value = $block->value();
				$preVars = Content::parentGroup('block-'.$block->value->name)->published()->ordered()->get();
				$name = $i;
				if(isset($block->value->name) && $block->value->name!='') $name = $block->value->name;
				$vars = [];
				foreach($preVars as $var){
                    $var->groupItems = $var->getGroupItems();
                    $var->groupItemsAll = $var->getGroupsItems();
                    $var->value = $var->value();
                    $vars[$var->name] = $var;
                }
				unset($preVars);
				$block->vars = $vars;
				$arr[$name] = $block;
				$i++;
			}
			$blocks = $arr;
//            dd($blocks);
			unset($arr);

//		Debug::info($blocks);


		}else{
			if(!$this->isAdmin) $stop = true;
		}

		if($stop){
			return view('templates.'.$meta->theme.'.stop', compact(
				'meta'
			));
		}else{
			return view('templates.'.$meta->theme.'.index', compact(
				'meta',
				'blocks'
			));
		}

	}

//	function adminIndex(){
//		$this->isAdmin = true;
//		$this->index();
//	}

}
