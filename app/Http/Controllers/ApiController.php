<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;

class ApiController extends Controller
{
    public function __construct()
    {
        // toDo:: something..
    }

    public function version(Request $request){

        return Response::json([
            'version' => '0.01',
            'action' => 'version',
            'model' => 'api',
            'request' => $request,
            'headers' => $request->server->getHeaders(),
            'REMOTE_ADDR' => $request->server->get('REMOTE_ADDR'),
            'REMOTE_HOST' => $request->server->get('REMOTE_HOST'),
        ]);
    }
}
