<?php

namespace App\Http\Controllers;

use App\ContentType;
use App\Http\Requests;
use Illuminate\Http\Request;
use Response;

class ApiContentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('key') && $request->has('value')){
            $relationship = '=';
            if($request->has('relationship')) $relationship = $request->get('relationship');
            $items = ContentType::where($request->get('key'), $relationship, $request->get('value'))->ordered()->get();
        }else{
            $items = ContentType::ordered()->get();
        }

        if($request->has('prepare') && $request->get('prepare')==true){
            $items = $this->prepareItems($items);
        }

        return Response::json([
            'items' => $items,
            'action' => 'index',
            'model' => 'contentType',
            'request' => $request->all()
        ]);
    }

    private function prepareItems($items){
        foreach($items as $item){
            $item->contents = $item->contents();
            $item->fields = $item->fields();
        }

        return $items;
    }

    public function contentTypeCreate(Request $request)
    {
        $log = [];

        ContentType::create($request->all());

        return Response::json([
            'action' => 'contentTypeCreate',
            'model' => 'contentType',
            'log' => $log,
            'request' => $request->all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function save(Request $request)
    {
        $log = [];
        $id = $request->get('id');
        $item = ContentType::find($id);

        /** @var TYPE_NAME $item */
        $item->update($request->all());

        $item = ContentType::find($id);

        return Response::json([
            'item' => $item,
            'request' => $request->all(),
            'action' => 'save',
            'model' => 'contentType',
            'log' => $log
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if($request->has('id')){
            ContentType::destroy($request->get('id'));
        }
        return Response::json([
            'action' => 'destroy',
            'model' => 'contentType',
            'request' => $request->all(),
        ]);
    }
}
