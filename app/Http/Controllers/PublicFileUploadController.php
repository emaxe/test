<?php

namespace App\Http\Controllers;

use App\Content;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Response;

class PublicFileUploadController extends Controller
{
    public function upload(Request $request)
    {
        $log = [];
        $status = 0;
        $destinationPath = "uploads";

        $id = Input::get('id');

        if (Input::hasFile('file'))
        {
            $log[] = "file present";
            $fileName = $id . "." . Input::file('file')->getClientOriginalExtension();
            Input::file('file')->move($destinationPath, $fileName);
            $content = Content::find($id);
            $value = $content->value();
            $value->file = $destinationPath."/".$fileName;
            $content->value = json_encode($value);
            $content->save();
        }
        else{
            $log[] = "file not present";
            $status = 1;
        }

        return Response::json([
            'status' => $status,
            'log' => $log,
            'action' => 'upload',
            'controller' => 'PublicFileUploadController',
            'id' => $id,
            'request' => $request,
        ]);
    }


}
