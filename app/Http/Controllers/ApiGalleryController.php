<?php namespace App\Http\Controllers;

use App\Gallery;
use App\GalleryItem;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Response;

class ApiGalleryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$items = Gallery::ordered()->get();

		$arr = [];
		foreach($items as $item){
			$item->items = $item->items();
			$arr[] = $item;
		}
		$items = $arr;

		return Response::json([
			'items' => $items,
			'action' => 'index',
			'model' => 'gallery',
			'request' => $request->all()
		]);
	}

	public function galleryCreate(Request $request){
		$log = [];

		$item = (array) $request->get('item');
		Gallery::create($item);

		return Response::json([
			'action' => 'galleryCreate',
			'model' => 'gallery',
			'log' => $log,
			'request' => $request->all()
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	public function galleryCreateImg(Request $request)
	{
		$log = [];

		GalleryItem::create($request->get('item'));

		return Response::json([
			'action' => 'galleryCreate',
			'model' => 'gallery',
			'log' => $log,
			'request' => $request->all()
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, Request $request)
	{
		$log = [];

		$item = Gallery::find($id);
		$item['items'] = $item->items();

		return Response::json([
			'action' => 'show',
			'model' => 'gallery',
			'log' => $log,
			'item' => $item,
			'request' => $request->all()
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateModel(Request $request)
	{
		$log = [];
		$item = Gallery::find($request->get('id'));

		/** @var TYPE_NAME $item */
		$item->update($request->get('item'));

		$rItems = $request->get('item')['items'];
		foreach($rItems as $rItem){
			if($rItem['id']>0){
				$item = GalleryItem::find($rItem['id']);
				$item->update($rItem);
			}else{

			}
		}

		$item = Gallery::find($request->get('id'));

		return Response::json([
			'item' => $item,
			'request' => $request->all(),
			'action' => 'update',
			'model' => 'gallery',
			'log' => $log
		]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function delete(Request $request)
	{
		if($request->has('id')){
			$id = $request->get('id');
			$gallery = Gallery::find($id);
			$items = $gallery->items();
			foreach($items as $item){
				$item->delete();
			}
			$gallery->delete();
		}
		return Response::json([
			'action' => 'delete',
			'model' => 'gallery',
			'request' => $request->all(),
		]);
	}

	public function deleteImg(Request $request)
	{
		if($request->has('item')){
			$id = $request->get('item')['id'];
			$galleryItem = GalleryItem::find($id);

			$galleryItem->delete();
		}
		return Response::json([
			'action' => 'deleteImg',
			'model' => 'gallery',
			'request' => $request->all(),
		]);
	}

}
