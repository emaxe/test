<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model {

    protected $fillable = [
        'id',
        'value',
        'description'
    ];

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'asc');
    }

    //==========================================================

    public function value($all=true)
    {
        if($this->value!=null){
            $o = json_decode($this->value);
        }else{
            $o = $this;
        }
        return $o;
    }

    //==========================================================

    public function fields()
    {
        return $this->hasMany('App\ContentField', 'select_id')->orderBy('order')->get();
    }

}
