<?php
/**
 * Created by PhpStorm.
 * User: emaxe
 * Date: 16.10.2015
 * Time: 13:21
 */

namespace App\Services;


class Sms
{
    private $curl;
    private $login;
    private $password;
    private $adminPhone;

    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
        $this->login = 't89034887640___';
        $this->password = '818793___';
        $this->adminPhone = '+79185680868';
    }

    public function balance(){
        $o = [
            'login' => $this->login,
            'password' => $this->password
        ];
        $balance = json_decode($this->curl->postJSON('http://api.prostor-sms.ru/messages/v2/balance.json', $o));
        return $balance;
    }

    public function api_version(){
        $o = [
            'login' => $this->login,
            'password' => $this->password
        ];
        $res = json_decode($this->curl->postJSON('http://api.prostor-sms.ru/messages/v2/version.json', $o));
        return $res;
    }

    public function senders(){
        $o = [
            'login' => $this->login,
            'password' => $this->password
        ];
        $res = json_decode($this->curl->postJSON('http://api.prostor-sms.ru/messages/v2/senders.json', $o));
        return $res;
    }

    public function send($number, $msg, $sender=0)
    {
        $balance = $this->balance();
        if ($balance->status == 'ok' && $balance->balance[0]->balance>1){
            $senders = $this->senders();
            if($senders->status == 'ok' && isset($senders->senders[$sender])){
                $o = [
                    'login' => $this->login,
                    'password' => $this->password,
                    'messages' => [
                        (object)[
                            'phone' => $number,
                            'sender' => $senders->senders[$sender]->name,
                            'clientId' => '1',
                            'text' => $msg,
                        ]
                    ],
                    "statusQueueName" => "myQueue"
                ];
                $res = json_decode($this->curl->postJSON('http://api.prostor-sms.ru/messages/v2/send.json', $o));
                return $res;
            }
        }
    }
}