<?php
/**
 * Created by PhpStorm.
 * User: emaxe
 * Date: 16.10.2015
 * Time: 13:23
 */

namespace App\Services;


class Curl
{
    public function postJSON($url, $o){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($o));
        curl_setopt($ch, CURLOPT_USERAGENT, 'CURL');
        $res = curl_exec($ch);
        if(!$res) {
            $error = curl_error($ch).'('.curl_errno($ch).')';
            $r =  $error;
            $r =  false;
        }
        else {
            $r = $res;
        }
        curl_close($ch);

        return $r;
    }
}