<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class ContentField extends Model {

    protected $fillable = [
        'id',
        'content_type_id',
        'group_type_id',
        'order',
        'name',
        'title',
        'type',
        'select_id',
        'select',
        'description',
        'editable',
        'default',
    ];

    //============================================

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'asc');
    }

    public function scopeName($query, $name)
    {
        $query->where('name', '=', $name);
    }

    public function scopeTypeID($query, $id)
    {
        $query->where('content_type_id', '=', $id);
    }

    //============================================

    public function type(){
        Log::info('CONTENT_FIELD -> TYPE() :=> '.$this->content_type_id.' - '.$this->id);
        return $this->belongsTo('App\ContentType', 'content_type_id')->get()[0];
    }

    public function select(){
//        return $this->belongsTo('App\Dictionary', 'select_id')->get()[0];
        Log::info('CONTENT_FIELD -> SELECT() :=> '.$this->select_id.' - '.$this->id);
        $arr = $this->belongsTo('App\Dictionary', 'select_id')->get();
//        Log::info('CONTENT_FIELD -> SELECT() :=> '.$this->select_id.' - '.$this->id." ".print_r($arr));
        if(count($arr)>0){
            return $arr[0];
        }else{
            return null;
        }
    }

}
