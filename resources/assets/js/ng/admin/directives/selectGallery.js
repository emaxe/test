/**
 * Created by emaxe on 19.09.2015.
 */

app.directive('selectGallery', function(Storage, rGallery){
    return {
        templateUrl: 'ng/directives/selectGallery.html',
        restrict: 'E',
        scope: {
            model: '='
        },
        link: function(scope, e, attrs){
            scope.Storage = Storage;
            console.log('selectGallery => ', scope.model);

            scope.items = Storage.galleries;

            rGallery.query(function(data){
                console.log('rGallery.query => ', data);
                scope.items = data.items;
                Storage.galleries = data.items;
            });
        }
    }
});