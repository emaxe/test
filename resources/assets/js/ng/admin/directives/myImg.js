/**
 * Created by emaxe on 19.09.2015.
 */

app.directive('myImg', function(){
    return {
        templateUrl: 'ng/directives/myImg.html',
        restrict: 'E',
        scope: {
            img: '=',
            alt: '='
        },
        link: function(scope, e, attrs){
            console.log('selectGallery => ', scope.img);
        }
    }
});