/**
 * Created by emaxe on 19.09.2015.
 */

app.directive('content', function($compile, RecursionHelper, helper, rContent, Storage){
    return {
        templateUrl: 'ng/directives/content.html',
        resctrict: 'E',
        scope: {
            item: '=',
            index: '=',
            editable: '=',
            fLoad: '=',
            reload: '=',
            change: '=?'
        },
        compile: function(element){
            return RecursionHelper.compile(element, function(scope, e, attrs, controller, transcludeFn){
                scope.Storage = Storage;
                scope.tinymceOptions = {
                    handle_event_callback: function (e) {
                        // put logic here for keypress
                    },
                    theme: "modern",
                    height : 375,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
                    ],
                    schema: "html5",
                    toolbar: "undo redo | fontselect fontsizeselect styleselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink image forecolor backcolor tablecontrols    | print preview media | responsivefilemanager | code",
                    statusbar: false,
                    language : 'ru',
                    image_advtab: true ,

                    relative_urls: false,
                    external_filemanager_path:"/tinymce/filemanager/",
                    filemanager_title:"Менеджер файлов" ,
                    external_plugins: { "filemanager" : "filemanager/plugin.min.js"}
                };

                $('.iframe-btn').fancybox({
                    'width'		: 900,
                    'height'	: 600,
                    'type'		: 'iframe',
                    'autoScale'    	: false
                });

                setTimeout(function(){
                    $('.iframe-btn').fancybox({
                        'width'		: 900,
                        'height'	: 600,
                        'type'		: 'iframe',
                        'autoScale'    	: false
                    });
                }, 200);

                setTimeout(function(){
                    $('.iframe-btn').fancybox({
                        'width'		: 900,
                        'height'	: 600,
                        'type'		: 'iframe',
                        'autoScale'    	: false
                    });
                }, 1000);

                scope.RFMInit = function(item){

                    console.info("F");
                    $('.iframe-btn').fancybox({
                        'width'		: 900,
                        'height'	: 600,
                        'type'		: 'iframe',
                        'autoScale'    	: false
                    });

                    setTimeout(function(){
                        console.info("F");
                        $('.iframe-btn').fancybox({
                            'width'		: 900,
                            'height'	: 600,
                            'type'		: 'iframe',
                            'autoScale'    	: false
                        });
                    }, 1000);

                    console.info('RFMInit - ', item, scope.item);
                    var f = function(field_id){
                        var o = document.getElementById(field_id);
                        var url = o.value;
                        for(var i=0; i<scope.item.fields.length;i++){
                            var field = scope.item.fields[i];
                            if(field_id == 'imgField_'+scope.item.id+'_'+field.id){
                                scope.item.value[field.name] = url;
                                console.info('Catch!!! ', field_id, o.value, field);
                                scope.$apply();
                                break;

                            }
                        }
                        console.log('Catch! ', field_id, o.value, scope.item);
                    };

                    for(var i=0; i<scope.item.fields.length;i++){
                        var field = scope.item.fields[i];

                        var value = scope.item.value[field.name];

                        if(value==null){
                            console.log('Value is NULL! ', field, value, field.default);
                            scope.item.value[field.name] = field.default;
                        }

                        if(field.type=='select'){
                            field.select = JSON.parse(field.select);
                        }

                        if(field.type=='animation'){
                            field.select = JSON.parse(field.select);
                            if(scope.item.value[field.name].delay==undefined) scope.item.value[field.name].delay=0;
                            if(scope.item.value[field.name].duration==undefined) scope.item.value[field.name].duration=0;
                        }

                        if(field.type=='img'){

                            var ff = true;
                            RFMCallbacks.map(function(o){
                                if(o.id==scope.item.id+'_'+field.id){
                                    o.go = f;
                                    ff = false;
                                }
                            });

                            if(ff){
                                console.log('--> ADDED ', ff, scope.item.id+'_'+field.id);
                                RFMCallbacks.push({
                                    id: scope.item.id+'_'+field.id,
                                    go: f
                                });
                            }

                        }
                    }
                };

                scope.addContent = function(ev, field){

                    helper.confirm({
                        title: 'Создание нового элемента',
                        text: 'Точно хотите создать новый элемент?',
                        yes: {
                            title: 'Создать',
                            onClick: function(){
                                scope.fLoad = true;
                                rContent.create({field:field, item_id: scope.item.id, type:'group'}, function(data){
                                    console.info('rContent.create - items', data);
                                    scope.fLoad = false;
                                    helper.showToast('Элемент создан!');
                                    scope.reload();
                                });
                            }
                        },
                        no: {
                            title: 'Отмена'
                        },
                        event: ev
                    });
                };

                scope.deleteContent = function(ev, item){

                    helper.confirm({
                        title: 'Удаление элемента',
                        text: 'Точно хотите удалить элемент?',
                        yes: {
                            title: 'Удалить',
                            onClick: function(){
                                scope.fLoad = true;
                                rContent.delete({id: item.id}, function(data){
                                    console.info('rContent.delete - items', data);
                                    scope.fLoad = false;
                                    helper.showToast('Элемент удален!');
                                    scope.reload();
                                });
                            }
                        },
                        no: {
                            title: 'Отмена'
                        },
                        event: ev
                    });
                };

                var checkItem = function(){
                    console.info('if(angular.isObject(scope.item)) -> ', scope.item, angular.isObject(scope.item));
                    if(angular.isObject(scope.item)){
                        scope.RFMInit();
                    }
                };

                checkItem();

                scope.$watch('change', function(){
                    checkItem();
                    scope.change = false;
                });

            });
        }
    }
});