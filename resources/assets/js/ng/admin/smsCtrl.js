/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('SmsCtrl', ['$scope', 'app', 'Config', 'Storage', 'rSms', 'helper', function($scope, app, Config, Storage, rSms, helper){
    console.log('SmsCtrl is ready!');
    var that = this;
    $scope.app = app;

    this.info = null;

    //========================
    this.menuItems = [
        {
            icon: 'ic_replay_white_24px.svg',
            tooltip: 'Обновить',
            label: 'Refresh',
            onClick: function(){
                that.reload();
            }
        }
    ];

    Storage.topMenuItems = this.menuItems;
    //========================

    this.init = function(f){
        that.fLoad = true;
        rSms.query(function(data){
            console.info('rSms.query - ', data);
            that.info = data.info;
            that.fLoad = false;
        });
    };


    this.reload = function(){
        that.init(function(data){
            helper.showToast('Обновлено');
        });
    };


    this.send = function(){
        rSms.send({phone:'89185680868',name:'Maksim', email:'',text:''}, function(data){
            console.info('rSms.send - ', data);
        })   ;
    };


    that.init();

}]);