/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('HomeCtrl', ['$scope', 'app', 'Config', 'Storage', 'rContent', 'helper', function($scope, app, Config, Storage, rContent, helper){
    console.log('homeCtrl is ready!');
    var that = this;
    $scope.app = app;

    this.items = null;

    //========================
    this.menuItems = [
        {
            icon: 'ic_replay_white_24px.svg',
            tooltip: 'Обновить',
            label: 'Refresh',
            onClick: function(){
                that.reload();
            }
        }
    ];

    Storage.topMenuItems = this.menuItems;
    //========================

    this.init = function(f){
        Storage.headerTabActive = 'order';
        that.setItemsGroup({
            key: 'group',
            value: 'order'
        });
        console.info('init', that.fSave);

        that.init_items(f);
    };

    this.init_items = function(f){
        that.fLoad = true;
        rContent.query({key: that.getItemsGroup().key, value: that.getItemsGroup().value}, function(data){
            that.fLoad = false;
            that.items = data.items;
            if(typeof(f)=='function'){
                f(data);
            }
            console.info('rContent.query - items', data);
        });
    };

    this.reload = function(){
        that.init_items(function(data){
            helper.showToast('Обновлено');
        });
    };

    this.getItemsGroup = function(){
        return Storage.homeBlock_itemGroup;
    };

    this.setItemsGroup = function(s){
        Storage.homeBlock_itemGroup = s;
    };


    this.myFilter = function(items) {
        var result = {};
        angular.forEach(items, function(value, key) {
            console.info('myFilter', value, key);
            if (value.value.is_new) {
                result[key] = value;
            }
        });
        return result;
    };

    this.itemOnClick = function(item){
        that.fLoad = true;
        rContent.update({id:item.id}, item, function(data){
            that.fLoad = false;
            helper.showToast('Сохранено');
            if(typeof(f)=='function'){
                f(data);
            }
            console.info('rContent.update - items', data);
        });
    };



    that.init();

}]);