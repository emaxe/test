/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('BlocksCtrl', ['$q', '$scope', '$timeout', 'app', 'Config', 'rBlocks', 'Storage', 'helper',
    function($q, $scope, $timeout, app, Config, rBlocks, Storage, helper){
        console.log('BlocksCtrl is ready!');
        var that = this;
        $scope.app = app;

        this.fLoad = false;
        this.items = null;
        this.item = null;
        this.fList = true;

        //========================
        this.menuItems = [
            {
                icon: 'ic_save_white_24px.svg',
                tooltip: 'Сохранить',
                label: 'Save',
                onClick: function(){
                    that.save();
                }
            },
            {
                icon: 'ic_replay_white_24px.svg',
                tooltip: 'Обновить',
                label: 'Refresh',
                onClick: function(){
                    that.init(function(data){
                        helper.showToast('Обновлено');
                    });
                }
            }
        ];

        Storage.topMenuItems = this.menuItems;
        //========================

        this.init = function(f){
            that.fLoad = true;
            rBlocks.query(function(data){
                that.fLoad = false;
                that.items = data.items;
                if(typeof(f)=='function'){
                    f(data);
                }
                console.info('rBlocks.query', data);
            });
        };

        this.init();

    }]);