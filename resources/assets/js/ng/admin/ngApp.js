/**
 * Created by emaxe on 10.07.2015.
 */

var app = angular.module('app', [
    'ngResource',
    'oc.lazyLoad',
    'ui.tinymce',
    'md.data.table',
    'ngMaterial',
    'RecursionHelper',
    //'angularModalService'
], function($interpolateProvider) {
    //$interpolateProvider.startSymbol('<%');
    //$interpolateProvider.endSymbol('%>');
});

app.config(function($sceDelegateProvider, $mdIconProvider, $httpProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
    ]);
    var csrfToken = $('meta[name=_token]').attr('content');
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = csrfToken;
    $httpProvider.defaults.headers.post['X-CSRF-Token'] = csrfToken;
    $httpProvider.defaults.headers.put['X-CSRF-Token'] = csrfToken;
    $httpProvider.defaults.headers.patch['X-CSRF-Token'] = csrfToken;

    $httpProvider.defaults.headers.common['Content-Type']= 'application/json; charset=UTF-8';
    $httpProvider.defaults.headers.common['App']= 'ng';
});

app.factory('app', function($ocLazyLoad, Storage, Config){
    console.log('app is init!');
    var that = this;

    this.options = {
        mainUrl: Config.mainUrl,
    };

    this.getJS = function(name,f){
        console.log('getJS - INDEX OF => ', Storage.jsLoadArray.indexOf(name));
        if(Storage.jsLoadArray.indexOf(name)<0){
            Storage.fLoad = true;
            console.log('getJS - try load => ', 'js/'+name+'.js');
            $ocLazyLoad.load('js/'+name+'.js').then(function() {
                console.log('LL - success1!', Storage.jsLoadArray);
                Storage.jsLoadArray.push(name);
                Storage.fLoad = false;
                console.log('LL - success2!', Storage.jsLoadArray);
                if(typeof(f)=='function'){
                    f();
                }
            }, function(e) {
                console.log(e);
            })
        }else{
            if(typeof(f)=='function'){
                f();
            }
        }
    };


    this.pages = { // страницы
        home: {
            name: 'home',
            src: 'ng/home.html',
            title: 'Главная',
            onLoad: function(){}
        },
        sms: {
            name: 'sms',
            src: 'ng/sms.html',
            title: 'SMS',
            onLoad: function(){}
        },
        meta: {
            name: 'meta',
            src: 'ng/meta.html',
            title: 'Настройка',
            onLoad: function(){}
        },
        menu: {
            name: 'menu',
            src: 'ng/menu.html',
            title: 'Меню',
            onLoad: function(){}
        },
        block: {
            name: 'block',
            src: 'ng/block.html',
            title: 'Блок',
            onLoad: function(){}
        },
        galleries: {
            name: 'galleries',
            src: 'ng/galleries.html',
            title: 'Галлереи',
            onLoad: function(){}
        },
        blocks: {
            name: 'blocks',
            src: 'ng/blocks.html',
            title: 'Блоки',
            onLoad: function(){}
        },
        settings: {
            name: 'settings',
            src: 'ng/settings.html',
            title: 'Настройки ROOT',
            onLoad: function(){}
        },
        blank: {
            name: 'blank',
            src: 'ng/blank.html',
            title: 'Blank page',
            onLoad: function(){}
        }
    };

    this.screen = {
        current: that.pages.blank,
        load: function(page, f){
            that.screen.preloader = false;
            if (typeof(that.screen.current.onLoad)=='function'){
                that.screen.current.onLoad();
            }
            if (typeof(f)=='function'){
                f();
            }
            console.log('JSLOAD = ',page);
            that.getJS('ng/admin/'+page.name+'Ctrl', function(){
                console.log('app getJS is success');
                that.screen.current = page;
                if (that.screen.current.name!=page.name){
                    that.screen.preloader = true;
                    that.apply();
                }
            });
        },
        unload: function(){
            that.screen.current = that.pages.blank;
        }
    };


    return this;
});

var RFMCallbacks = [];
window.RFMCallbacks = RFMCallbacks;

function responsive_filemanager_callback(field_id){
    console.log('Callbacks => ', RFMCallbacks, field_id);
    RFMCallbacks.map(function(item){
        if('imgField_'+item.id==field_id){
            item.go(field_id);
        }
    });
}


$('.iframe-btn').fancybox({
    'width'		: 900,
    'height'	: 600,
    'type'		: 'iframe',
    'autoScale'    	: false
});