/**
 * Created by emaxe on 10.07.2015.
 */
var app = angular.module('app', []);

app.controller('DialogEditTypeCtrl', function($scope, $mdDialog, rContent, rContentField, helper){
    console.log('dialogEditTypeCtrl is ready!');
    var that = this;
    $scope.app = app;
    this.InScope = $scope.InScope;

    this.items = null;
    //this.field = $scope.InScope.field;
    this.fLoad = true;

    this.item = that.InScope.item;

    this.init = function(){

    };

    this.confirm = function(){
        var f = true;
        var s = '';
        if(that.item.id==''){
            f = false;
            s += '[ID] ';
        }
        if(that.item.name==''){
            f = false;
            s += '[Имя] ';
        }
        if(that.item.title==''){
            f = false;
            s += '[Заголовок] ';
        }
        if(f) {
            that.answer(that.item)
        }else{
            helper.showToast( 'Поля '+s+' обязательны для заполнения!');
        }
    };

    this.hide = function() {
        $mdDialog.hide();
    };
    this.cancel = function() {
        $mdDialog.cancel();
    };
    this.answer = function(answer) {
        $mdDialog.hide({
            state:'ok',
            item: answer
        });
    };

    that.init();

});