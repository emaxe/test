/**
 * Created by emaxe on 10.07.2015.
 */
//if(app==undefined){
//    var app = angular.module('app', []);
//}

app.controller('mainCtrl', function($scope, app, Storage, $mdSidenav, blocksList, Config, rDictionary){
    console.log('mainCtrl is ready!');
    var that = this;
    window.scope = $scope;
    $scope.app = app;
    $scope.Storage = Storage;
    $scope.imagePath = 'i/washedout.png';

    this.init = function(){


        blocksList.init(function(){
            that.blocks = blocksList.items;
        });

        rDictionary.get(1, function(data){
            that.fLoad = false;
            Storage.fonts = data.items[0].value;
            console.info('rDictionary.query - fonts', data, Storage.fonts);
        });
    };



    this.toggleSidenav = function(menuId) {
        $mdSidenav(menuId).toggle();
    };

    app.onApply = function(){
        $scope.$apply();
    };

    app.screen.load(app.pages.home);


    this.goToBlock = function(block){
        Storage.block = block;
        if(app.screen.current.name != 'block'){
            that.goToPage('block', true, function(){
                //Storage.initBlock();
            });
        }else{
            Storage.initBlock();
        }
    };


    this.goToPage = function(pageName, closeNav, f){
        app.screen.load(app.pages[pageName], f);
        if( closeNav ){
            $mdSidenav('left').close();
        }
    };

    this.goToLink = function(link){
        console.log(link);
        location.replace(link);
    };

    this.checkRoot = function(){
        if(Config.meta['_user_role']==0){
            Storage.isRoot = true;
        }
    };

    that.init();

    console.log('mainCtrl is end!');

});