/**
 * Created by emaxe on 13.07.2015.
 */

app.factory('rContent',['$resource', 'Config', function($resource, Config){
    return $resource('apiContent/:id', null, {
        update: {
            method: 'POST',
            url: 'apiContentUpdate',
            isArray: false
        },
        query: {
            method: 'GET',
            isArray: false
        },
        save: {
            method:'POST',
            url: 'apiContentSave',
            isArray: false
        },
        save2: {
            method:'POST',
            url: 'apiContentSave2',
            isArray: false
        },
        create: {
            method:'POST',
            url: 'apiContentCreate',
            isArray: false
        },
        create2: {
            method:'POST',
            url: 'apiContentCreate2',
            isArray: false
        },
        delete: {
            method:'POST',
            url: 'apiContentDelete',
            isArray: false
        }
    });
}]);

app.factory('rContentField',['$resource', 'Config', function($resource, Config){
    return $resource('apiContentField/:id', null, {
        update: {
            method: 'PUT'
        },
        query: {
            method: 'GET',
            isArray: false
        },
        save: {
            method:'POST',
            url: 'apiContentFieldSave',
            isArray: false
        },
        create: {
            method:'POST',
            url: 'apiContentFieldCreate',
            isArray: false
        },
        delete: {
            method:'POST',
            url: 'apiContentFieldDelete',
            isArray: false
        }
    });
}]);

app.factory('rContentType',['$resource', 'Config', function($resource, Config){
    return $resource('apiContentType/', null, {
        update: {
            method: 'PUT'
        },
        query: {
            method: 'GET',
            isArray: false
        },
        save: {
            method:'POST',
            url: 'apiContentTypeSave',
            isArray: false
        },
        create: {
            method:'POST',
            url: 'apiContentTypeCreate',
            isArray: false
        },
        delete: {
            method:'POST',
            url: 'apiContentTypeDelete',
            isArray: false
        }
    });
}]);

app.factory('rGallery',['$resource', 'Config', function($resource, Config){
    return $resource('apiGallery/:id', null, {
        update: {
            method: 'POST',
            url: 'apiGalleryUpdate',
            isArray: false
        },
        query: {
            method: 'GET',
            isArray: false
        },
        save: {
            method:'POST',
            url: 'apiGallerySave',
            isArray: false
        },
        create: {
            method:'POST',
            url: 'apiGalleryCreate',
            isArray: false
        },
        createImg: {
            method:'POST',
            url: 'apiGalleryCreateImg',
            isArray: false
        },
        delete: {
            method:'POST',
            url: 'apiGalleryDelete',
            isArray: false
        },
        deleteImg: {
            method:'POST',
            url: 'apiGalleryDeleteImg',
            isArray: false
        }
    });
}]);

app.factory('rBlocks',['$resource', 'Config', function($resource, Config){
    return $resource('apiBlocks/:id', null, {
        update: {
            method: 'POST',
            url: 'apiBlocksUpdate',
            isArray: false
        },
        query: {
            method: 'GET',
            isArray: false
        },
        save: {
            method:'POST',
            url: 'apiBlocksSave',
            isArray: false
        },
    });
}]);

app.factory('rDictionary',['$resource', 'Config', function($resource, Config){
    return $resource('apiDictionary/:id', null, {
        query: {
            method: 'GET',
            isArray: false
        },
    });
}]);

app.factory('rSms',['$resource', 'Config', function($resource, Config){
    return $resource('apiSms/:id', null, {
        query: {
            method: 'GET',
            isArray: false
        },
        send: {
            method:'POST',
            url: 'apiSmsSend',
            isArray: false
        },
        info: {
            method:'POST',
            url: 'apiSmsInfo',
            isArray: false
        }
    });
}]);