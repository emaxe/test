/**
 * Created by emaxe on 13.07.2015.
 */

app.directive('menuLine', function(Storage){
    return {
        templateUrl: 'ng/directives/menuLine.html',
        restrict: 'E',
        scope: {
            items: '='
        },
        link: function(scope, e, attrs){
            scope.Storage = Storage;
            console.log('DML => ', scope.items);

            //scope.$watch(Storage, function(){
            //    scope.Storage = Storage;
            //})
        }
    }
});