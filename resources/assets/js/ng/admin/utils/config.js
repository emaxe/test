/**
 * Created by emaxe on 13.07.2015.
 */

app.factory('Config',function(){
    var that = this;

    this.meta = {};

    this.meta.token = $('meta[name=_token]').attr('content');
    this.meta.host = $('meta[name=_app_host]').attr('content');
    this.meta.userRole = $('meta[name=_user_role]').attr('content');


    var metas = document.getElementsByTagName('meta');
    for (var i=0; i<metas.length; i++) {
        var name = metas[i].getAttribute("name");
        if(name!=null){
            that.meta[name] = metas[i].getAttribute("content");
        }
    }

    this.mainUrl = 'http://admin.'+this.meta.host;
    this.publicUrl = 'http://'+this.meta.host;

    return this;
});