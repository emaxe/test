/**
 * Created by emaxe on 10.07.2015.
 */

//if(app==undefined){
//    var app = angular.module('app', []);
//}

app.factory('Storage', function(){
    var that = this;

    this.subHeader = '';
    this.topMenuItems = null;
    this.headerTabActive = null;

    this.fLoad = false;

    this.jsLoadArray = [];


    this.meta = null;
    this.menu = null;

    this.initBlock = function(){};

    this.galleries = [];

    this.isRoot = false;


    this.fieldTypes = {
        string: {
            name: 'string',
            title:'Строка'
        },
        text: {
            name: 'text',
            title:'Текст'
        },
        int: {
            name: 'int',
            title:'Число'
        },
        group: {
            name: 'group',
            title:'Группа'
        },
        img: {
            name: 'img',
            title:'Картинка'
        },
        boolean: {
            name: 'boolean',
            title:'Логическое значение'
        },
        gallery: {
            name: 'gallery',
            title:'Галлерея'
        },
        html: {
            name: 'html',
            title:'HTML'
        },
        phone: {
            name: 'phone',
            title:'Телефон'
        },
        email: {
            name: 'email',
            title:'E-mail'
        },
        font:{
            name: 'font',
            title: 'Шрифт'
        }
    };

    return this;
});