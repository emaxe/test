/**
 * Created by emaxe on 07.10.2015.
 */


app.factory('blocksList', function(rContent){
    var that = this;

    this.fLoad = false;

    this.items = [];

    this.init = function (f){
        that.fLoad = true;
        rContent.query({key: 'group', value: 'blocks-items'}, function(data){
            that.fLoad = false;
            for(var i=0;i<data.items.length;i++){
                that.items.push(data.items[i].value);
            }
            //that.items = data.items;
            if(typeof(f)=='function'){
                f(data);
            }
            console.info('rContent.query - blocks', data);
        });
    };



    return this;
});