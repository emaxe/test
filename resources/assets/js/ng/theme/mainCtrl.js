/**
 * Created by emaxe on 10.07.2015.
 */

app.controller('mainCtrl', function($window, $timeout, $scope, app, toastr, rContent, rGallery, rSms, Upload){
    console.log('mainCtrl is ready!');
    var that = this;
    $scope.app = app;

    fundament_id = 0;
    showOverlay = false;
    this.windowSize = {
        w:0,
        h:0
    };



    this.codeGen = function()
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for( var i=0; i < 5; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    };


    this.init = function(){

    };

    this.os = {
        is_ShowModal: false,
        name: "",
        phone: "",
        email: '',
        contacts: "",
        text: "",
        errorMsg: "",
        fSms: false,
        fToast: false,
        showModal: function(){
            that.os.clear();
            that.os.is_ShowModal = true;
        },
        hideModal: function(){
            that.os.is_ShowModal = false;
        },
        clear: function(){
            if(!that.os.fSms && !that.os.fToast){
                that.os.name = '';
                that.os.phone = '';
                that.os.email = '';
                that.os.contacts = '';
                that.os.text = '';
                that.os.hideModal();
            }
        },
        send: function(ev, file){
            ev.preventDefault();
            ev.stopPropagation();
        // send: function(file){

            if(file==undefined) file = null;

            toastr.clear();
            toastr.info('', 'Заявка обрабатывается..');

            if(that.os.phone!='' || that.os.email!=''){
                that.os.fSms = true;
                that.os.fToast = true;

                rSms.send(that.os, function(data){
                    console.info('rSms.send - ', data) ;
                    that.os.fSms = false;
                    that.os.clear();
                });

                rContent.addToast({item: that.os}, function(data){
                    console.info('rContent.addToast - form', data);

                    that.os.fToast = false;

                    toastr.clear();
                    toastr.info('Мы свяжемся с вами в ближайшее время!', 'Заявка принята');

                    that.os.clear();



                    var id = data.item.id;

                    if(file!=undefined && file!=null){
                        file.upload = Upload.upload({
                            //url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                            url: 'http://zabor-rostov.ru/publicFileUpload/upload',
                            data: {file: file, id: id},
                        });

                        file.upload.then(function (response) {
                            $timeout(function () {
                                file.result = response.data;
                            });
                        }, function (response) {
                            if (response.status > 0)
                                that.os.errorMsg = response.status + ': ' + response.data;
                        }, function (evt) {
                            // Math.min is to fix IE which reports 200% sometimes
                            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }

                });
                console.info(that.os);
                // that.hidePopup();
            }else{
                toastr.clear();
                toastr.error('Ошибка!', 'Телефон либо email должны быть указаны!');
            }
        }
    };

    this.order = {
        name: '',
        phone: '',
        text: '',
        onClick: function(ev){
            ev.preventDefault();
            toastr.clear();
            toastr.info('', 'Заявка обрабатывается..');

            if(that.order.phone!=''){

                rSms.send(that.order, function(data){
                    console.info('rSms.send - ', data) ;
                });

                rContent.addToast({item: that.order}, function(data){
                    console.info('rContent.addToast - form', data);

                    toastr.clear();
                    toastr.info('Мы свяжемся с вами в ближайшее время!', 'Заявка принята');
                    that.order.name = '';
                    that.order.phone = '';
                    that.order.text = '';

                });
                console.info(that.order);
                that.hidePopup();
            }else{
                toastr.clear();
                toastr.error('Ошибка!', 'Телефон должен быть указан!');
            }


        }
    };

    this.selectMaterial = function(ev, e){
        var block = document.getElementById('block-promo1');
        var items = block.getElementsByClassName('item');
        var itemContents = block.getElementsByClassName('item-content');

        // console.info(items, itemContents);

        for(var i=0;i<items.length;i++){
            items[i].classList.remove('active');
        }

        for(var i=0;i<itemContents.length;i++){
            itemContents[i].classList.remove('active');
        }

        // console.info(ev.target);
        var item = ev.target;
        var i = item.getAttribute('i');
        console.info(item, item.getAttribute('i'), i);
        var itemContent = document.getElementById('itemContent'+item.getAttribute('i'));
        item.classList.add('active')
        itemContent.classList.add('active')
    };

    that.init();

    console.log('mainCtrl is end!');

});