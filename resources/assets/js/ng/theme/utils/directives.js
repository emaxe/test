/**
 * Created by emaxe on 22.09.2015.
 */

app.directive('ngPartners', function(){
    return {
        restrict: 'EA',
        scope: {},
        link: function(scope, e, attrs){
            console.info('ngPartners => ', e[0]);
            scope.line = e[0].getElementsByClassName('items-line')[0];
            scope.btns = e[0].getElementsByClassName('btns')[0];
            scope.btnLeft = scope.btns.getElementsByClassName('btn-left')[0];
            scope.btnRight = scope.btns.getElementsByClassName('btn-right')[0];

            var init = function(){
                scope.items = scope.line.getElementsByClassName('item');
                console.info('ngPartners - init => ', scope.els);

                if(scope.items.length < 6){
                    scope.btns.style.display = 'none';
                }

                var w = 0;
                var f =true;

                for(var i=0;i<scope.items.length;i++){
                    // scope.items[i].style.position = 'absolute';
                    // scope.items[i].style.left = w+'px';
                    w += scope.items[i].offsetWidth+18;

                    if(scope.items[i].offsetWidth<20){
                        f = false;
                        setTimeout(function(){
                            init();
                        }, 200);
                        break;
                    }
                }

                scope.line.style.width = w+'px';
                scope.w = w;

            };

            var setEase = function(fEase){
                if(fEase){
                    scope.line.classList.add('ease');
                }else{
                    scope.line.classList.remove('ease');
                }
            };

            scope.goLeft = function(){
                console.info('ngPartners - goLeft => ');
                setEase(true);
                scope.line.style.left = (scope.line.offsetLeft-scope.items[0].offsetWidth)+'px';
                setTimeout(function(){
                    setEase(false);
                    scope.line.style.left = '0px';
                    var e_item = scope.items[0];
                    scope.line.removeChild(e_item);
                    scope.line.insertBefore(e_item, null);
                    scope.items = scope.line.getElementsByClassName('item');
                },400);
            };

            scope.goRight = function(){
                console.info('ngPartners - goRight => ');
                setEase(true);
                scope.line.style.left = (scope.line.offsetLeft+scope.items[0].offsetWidth)+'px';
                setTimeout(function(){
                    setEase(false);
                    scope.line.style.left = '0px';
                    var e_item = scope.items[scope.items.length-1];
                    scope.line.removeChild(e_item);
                    scope.line.insertBefore(e_item,scope.items[0]);
                    scope.items = scope.line.getElementsByClassName('item');
                },400);
            };

            scope.btnLeft.addEventListener('click', function(e){
                scope.goLeft();
            });

            scope.btnRight.addEventListener('click', function(e){
                scope.goRight();
            });

            init();
        }
    }
});

app.directive('ngWork', function(rContent, rGallery){
    return {
        restrict: 'EA',
        scope: {},
        link: function(scope, e,attrs){
            console.info('ngWork 0 => ', e[0]);

            var galleries = {};
            var isDone = false;
            var e_popup = document.getElementById('work-popup');
            var is_anima = false;

            e_popup.parentNode.removeChild(e_popup);
            document.body.appendChild(e_popup);
            var scrollPos = 0;

            var e_popup_wrap = e_popup.getElementsByClassName('wp-wrap')[0];
            var e_popup_title = e_popup.getElementsByClassName('wp-title')[0];
            var e_popup_text = e_popup.getElementsByClassName('wp-text')[0];
            var e_popup_items = e_popup.getElementsByClassName('wp-items')[0];
            var e_popup_items_wrap = e_popup.getElementsByClassName('wp-items-wrap')[0];
            var e_x = e_popup.getElementsByClassName('x')[0];
            var e_left = e_popup.getElementsByClassName('wp-left')[0];
            var e_right = e_popup.getElementsByClassName('wp-right')[0];

            var items = e[0].getElementsByClassName('item');
            var loaded_galleries_count = 0;
            console.info('ngWork items => ', items);



            var init = function(){

                for(var i=0;i<items.length;i++){
                    items[i].addEventListener('click', function(ev){
                        console.info('ngWork-click - ', ev);

                        var eItem = ev.target;
                        while(eItem.parentNode!=document.body && !eItem.classList.contains('item')){
                            eItem = eItem.parentNode;
                        }

                        var gID = eItem.getAttribute('gallery-id');
                        var gTitle = eItem.getAttribute('gallery-title');
                        var index = eItem.getAttribute('index');

                        console.info('--> ', items[index]);


                        // e_popup.classList.add('transition');
                        e_popup.classList.add('show');

                        console.info('-->> GALLERIES = ', galleries, gID, galleries[gID],galleries[gID].items.length);

                        e_popup_title.innerText = gTitle;
                        // e_popup_text.innerText = galleries[gID].text;
                        e_popup_items_wrap.innerHTML = '';
                        e_popup_items_wrap.style.width = ( galleries[gID].items.length*204 + 10 * (galleries[gID].items.length) ) + 'px';

                        if(galleries[gID].items.length<=4){
                            // e_next.style.display = 'none';
                            e_right.style.display = 'none';
                            e_left.style.display = 'none';
                            // e_prev.style.display = 'none';
                        }

                        for(var ii=0;ii<galleries[gID].items.length;ii++){
                            var e_img = document.createElement('DIV');
                            e_img.style.backgroundImage = 'url('+galleries[gID].items[ii].img+')';
                            e_img.classList.add('wp-item');

                            var e_a = document.createElement('A');
                            e_a.href = galleries[gID].items[ii].img;
                            e_a.setAttribute('rel', "group1");
                            e_a.classList.add('f-img');

                            e_img.appendChild(e_a);

                            e_popup_items_wrap.appendChild(e_img);
                        }

                        // document.body.classList.add('hide-sections');
                        // scrollPos = document.body.scrollTop;


                        $(".f-img").fancybox({
                            'transitionIn'	:	'elastic',
                            'transitionOut'	:	'elastic',
                            'autoDimensions'	: true,
                            'speedIn'		:	600,
                            'speedOut'		:	200,
                            'overlayShow'	:	true
                        });

                    });

                    var gID = items[i].getAttribute('gallery-id');

                    rGallery.get({id:gID}, function(data){
                        console.info('ngWork -> -> ', data.item, galleries);
                        loaded_galleries_count++;

                        // if(data.item != undefined && galleries[data.item.id]==undefined){
                        if(data.item != undefined){
                            galleries[data.item.id] = data.item;
                            console.info('ngWork -> -> -> ', galleries);
                        }else {
                            console.error('ngWork 1 -> not LOADED');
                        }

                        if(loaded_galleries_count == items.length){
                            go();
                        }
                    });
                }


            };

            var go = function(){


                e_popup.addEventListener('click', function(ev){
                    console.info('ngWork-click - e_popup - ', ev);
                    if(ev.target == e_popup){
                        close();
                    }
                });

                e_x.addEventListener('click', function(ev){
                    console.info('ngWork-click - e_x - ', ev);

                    close();
                });

                e_right.addEventListener('click', function(ev){
                    console.info('ngWork-click - e_next - ', ev);

                    if(!is_anima){
                        e_popup_items_wrap.classList.remove('transition');
                        e_popup_items_wrap.style.marginLeft = - (204 + 10) + 'px';
                        var imgs = e_popup_items_wrap.getElementsByClassName('wp-item');
                        var img = imgs[imgs.length-1];
                        e_popup_items_wrap.removeChild(img);
                        e_popup_items_wrap.insertBefore(img,imgs[0]);
                        // e_popup_items_wrap.classList.add('transition');

                        setTimeout(function(){
                            e_popup_items_wrap.classList.add('transition');
                            e_popup_items_wrap.style.marginLeft = '0px';
                            is_anima = true;

                            setTimeout(function(){
                                e_popup_items_wrap.classList.remove('transition');

                                is_anima = false;
                            },400);
                        },30);

                    }
                });

                e_left.addEventListener('click', function(ev){
                    console.info('ngWork-click - e_prev - ', ev);

                    if(!is_anima){
                        e_popup_items_wrap.classList.add('transition');
                        e_popup_items_wrap.style.marginLeft = - (204 + 10) + 'px';
                        is_anima = true;

                        setTimeout(function(){
                            e_popup_items_wrap.classList.remove('transition');
                            e_popup_items_wrap.style.marginLeft = '0px';

                            var imgs = e_popup_items_wrap.getElementsByClassName('wp-item');
                            var img = imgs[0];
                            e_popup_items_wrap.removeChild(imgs[0]);
                            e_popup_items_wrap.appendChild(img);

                            is_anima = false;
                        },400);

                    }


                });
            };

            var close = function(){
                e_popup.classList.add('transition');
                e_popup.classList.remove('show');
                // document.body.classList.remove('hide-sections');
                // document.body.scrollTop = scrollPos;
            };

            init();
        }
    }
});

app.directive('ngGallery', function(rContent, rGallery){
    return {
        restrict: 'EA',
        scope: {
            galleryId: '='
        },
        link: function(scope, e, attrs){
            console.info('ngGallery => ', e[0]);
            scope.w = 0;
            scope.f = true;

            scope.el = e[0].getElementsByClassName('imgs')[0];

            var init = function(){
                scope.els = scope.el.getElementsByClassName('img');
                console.info('ngGallery - init => ', scope.els);

                var w = 0;
                var f =true;

                for(var i=0;i<scope.els.length;i++){
                    scope.els[i].style.left = w+'px';
                    w += scope.els[i].offsetWidth+1;

                    if(scope.els[i].offsetWidth<20){
                        f = false;
                        setTimeout(function(){
                            init();
                        }, 200);
                        break;
                    }
                }

                scope.el.style.width = w+'px';
                scope.w = w;

                if(f){
                    scope.go();
                }

            };

            scope.go = function(){
                //console.info('ngGallery - go => ', scope.els);

                if(scope.f){
                    for(var i=0;i<scope.els.length;i++){
                        if(scope.els[i].offsetLeft+scope.els[i].offsetWidth<0){
                            scope.els[i].style.left = (scope.w-scope.els[i].offsetWidth)+'px';
                        }else{
                            scope.els[i].style.left = (scope.els[i].offsetLeft-1)+'px';
                        }
                    }
                }

                setTimeout(function(){
                    scope.go();
                }, 10);
            };

            scope.el.addEventListener('mouseenter', function(e){
                scope.f = false;
            });

            scope.el.addEventListener('mouseleave', function(e){
                scope.f = true;
            });

            init();
        }
    }
});

app.directive('ngSlider', function(){
    return {
        restrict: 'A',
        transclude: false,
        scope: {
            onLeft: '=',
            onRight: '='
        },
        link: function(scope, e, attrs){
            console.info('ngSlider is ready!', e);

            //scope.slider = document.getElementsByClassName();
            window.slider = e[0];
            scope.slider = e[0];
            scope.k = 0;

            scope.slides = scope.slider.getElementsByClassName('slide');
            scope.n = scope.slides.length;



            // Set slider dimentions like current slide dimensions
            scope.setSliderDim = function(){
                scope.slider.style.height = scope.slides[scope.k].offsetHeight + 'px';
            };

            scope.showActiveSlide = function(){
                for(var i=0; i<scope.n; i++){
                    if(i==scope.k){
                        //scope.slides[i].style.opacity = 1;
                        //scope.slides[i].style.display = 'block';
                        if(!scope.slides[i].classList.contains('active')) scope.slides[i].classList.add('active')
                    }else{
                        //scope.slides[i].style.opacity = 0;
                        if(scope.slides[i].classList.contains('active')) scope.slides[i].classList.remove('active');
                        //scope.slides[i].style.display = 'none';
                        //setTimeout(function(){
                        //    scope.slides[i].style.display = 'none';
                        //}, 400);
                    }
                }
                scope.setSliderDim();
            };
            scope.showActiveSlide();






            scope.$on('onLeft', function(){
                console.log('Slide left!');

                if(scope.k>0){
                    scope.k--;
                }else{
                    scope.k = scope.n-1;
                }

                scope.showActiveSlide();
            });

            scope.$on('onRight', function(){
                console.log('Slide right!');

                if(scope.k<scope.n-1){
                    scope.k++;
                }else{
                    scope.k = 0;
                }

                scope.showActiveSlide();
            });
        }
    }
});

app.directive('onSizeChanged', ['$window', function ($window) {
    return {
        restrict: 'A',
        scope: {
            onSizeChanged: '&'
        },
        link: function (scope, $element, attr) {
            var element = $element[0];

            cacheElementSize(scope, element);
            $window.addEventListener('resize', onWindowResize);

            function cacheElementSize(scope, element) {
                scope.cachedElementWidth = element.offsetWidth;
                scope.cachedElementHeight = element.offsetHeight;
            }

            function onWindowResize() {
                var isSizeChanged = scope.cachedElementWidth != element.offsetWidth || scope.cachedElementHeight != element.offsetHeight;
                if (isSizeChanged) {
                    var expression = scope.onSizeChanged(element.offsetWidth, element.offsetHeight);
                    expression();
                }
            };
        }
    }
}]);