/**
 * Created by emaxe on 13.07.2015.
 */

app.factory('rContent',['$resource', 'Config', function($resource, Config){
    return $resource('publicApiContent/:id', null, {
        query: {
            method: 'GET',
            isArray: false
        },
        addToast: {
            method:'POST',
            url: 'publicApiAddToast',
            isArray: false
        }
    });
}]);

app.factory('rGallery',['$resource', 'Config', function($resource, Config){
    return $resource('publicApiGallery/:id', null, {
        query: {
            method: 'GET',
            isArray: false
        }
    });
}]);

app.factory('rSms',['$resource', 'Config', function($resource, Config){
    return $resource('publicApiSms/:id', null, {
        send: {
            method:'POST',
            url: 'publicApiSmsSend',
            isArray: false
        }
    });
}]);