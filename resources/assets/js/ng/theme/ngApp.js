/**
 * Created by emaxe on 10.07.2015.
 */

var app = angular.module('app', ['ngResource', 'toastr', 'ngMask', 'ngFileUpload']);

app.config(function(toastrConfig) {
    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,
        newestOnTop: true,
        positionClass: 'toast-bottom-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    });
});

app.factory('app', function(){
    console.log('app is init!');
    var that = this;

    return this;
});



