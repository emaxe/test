<!doctype html>
<html lang="ru" ng-app="app">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots" content="noindex"/>

    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="_user_role" content="{!! $user->role or 3 !!}"/>
    <meta name="_app_host" content="{!! env('APP_HOST', false) !!}"/>

    <title>TEST - ADMIN</title>

    {{--<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500' rel='stylesheet' type='text/css'>--}}
    <link rel="stylesheet" href="{{ asset("css/app.css") }}">
    <link rel="stylesheet" href="{{ asset("css/bowerLibs.css") }}">
    <link rel="stylesheet" href="{{ asset("css/ngMaterial.css") }}">
    {{--<link rel="stylesheet" href="{{ asset("css/nodeLibs.css") }}">--}}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body ng-controller="mainCtrl as main" >


<div id="wrapper">

    <md-toolbar layout="row">
        <div class="md-toolbar-tools">
            <md-button ng-click="main.toggleSidenav('left')" hide-gt-md class="md-icon-button">
                <span class="fa fa-sliders"></span>
            </md-button>
            <a href="{{ env('APP_URL', '/')}}" class="margin-right-5" title="Перейти на сайт">
                <span class="eico eico-uniEC13"></span>
            </a>
            <h1><span ng-bind="app.screen.current.title+' '+Storage.subHeader"></span></h1>
            <span flex></span>
            <mb-button ng-if="Storage.fLoad" class="md-icon-button">
                <span class="fa fa-refresh fa-spin"></span>
            </mb-button>
            <span ng-if="Storage.topMenuItems!=null">
                <menu-line items="Storage.topMenuItems"></menu-line>
            </span>
        </div>
    </md-toolbar>

    <section id="wr-section" layout="row" flex>

        @include('admin.parts._angular-nav')

        <md-content md-swipe-left="main.onSwipeLeft($event)" id="content" layout-padding class="width-100" class="autoScroll">
            <el-screen-content id="screen-content" ng-include="app.screen.current.src" onload="app.screen.current.onload()"></el-screen-content>
        </md-content>

    </section>


    <div id="page-wrapper">
        @yield('content')

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<script src="{{ asset('tinymce/tinymce.min.js') }}"></script>

<script src="{{ asset('js/bowerLibs.js') }}"></script>

<script src="{{ asset('js/angular.js') }}"></script>
<script src="{{ asset('js/angular-ui-tinymce.js') }}"></script>
<script src="{{ asset('js/ngApp.js') }}"></script>


@yield('scripts')

</body>
</html>