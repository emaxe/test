<md-sidenav class="md-sidenav-left md-whiteframe-z2" md-component-id="left" md-is-locked-open="$mdMedia('gt-md')">
    <md-content layout-padding ng-controller="LeftCtrl">
        <md-list>
            <md-list-item ng-class="{'active':app.screen.current.name=='home'}" class="md-3-line" ng-click="main.goToPage('home', true)">
                <span class="eico eico-uniEB01"></span>
                <div class="md-list-item-text">
                    <h3>Главная</h3>
                </div>
            </md-list-item>
            <md-divider ></md-divider>

            <md-divider ></md-divider>
            <md-list-item ng-class="{'active':app.screen.current.name=='galleries'}" class="md-3-line" ng-click="main.goToPage('galleries', true)">
                <span class="eico eico-uniEBC0"></span>
                <div class="md-list-item-text">
                    <h3>Галлереи</h3>
                </div>
            </md-list-item>

<!--            <md-divider ></md-divider>-->
<!--            <md-list-item ng-class="{'active':app.screen.current.name=='blocks'}" class="md-3-line" ng-click="main.goToPage('blocks', true)">-->
<!--                <span class="eico eico-uniEB21"></span>-->
<!--                <div class="md-list-item-text">-->
<!--                    <h3>Блоки</h3>-->
<!--                </div>-->
<!--            </md-list-item>-->

            <md-divider ></md-divider>
            <md-divider ></md-divider>
            <md-divider ></md-divider>

            <md-subheader class="md-no-sticky">Блоки сайта</md-subheader>
            <md-list-item
                ng-repeat="block in main.blocks"
                ng-class="{'active':app.screen.current.name=='block'}"
                class="md-3-line"
                ng-click="main.goToBlock(block)">
                <span class="eico eico-uniEB3F"></span>
                <div class="md-list-item-text">
                    <h3 ng-bind="block.title"></h3>
                </div>
                <md-divider ></md-divider>
            </md-list-item>
            <md-divider ></md-divider>
            <md-divider ></md-divider>
            <md-divider ></md-divider>



            <md-subheader class="md-no-sticky">Дополнительно</md-subheader>

            <md-list-item ng-class="{'active':app.screen.current.name=='sms'}" class="md-3-line" ng-click="main.goToPage('sms', true)">
                <span class="eico eico-iphone"></span>
                <div class="md-list-item-text">
                    <h3>SMS</h3>
                </div>
            </md-list-item>

            <md-divider ></md-divider>

            <md-list-item ng-mouseover="main.checkRoot()" ng-class="{'active':app.screen.current.name=='meta'}" class="md-3-line" ng-click="main.goToPage('meta', true)">
                <span class="eico eico-cog"></span>
                <div class="md-list-item-text">
                    <h3>Настройки</h3>
                </div>
            </md-list-item>


            <md-divider ng-if="Storage.isRoot"></md-divider>
            <md-list-item ng-if="Storage.isRoot" ng-class="{'active':app.screen.current.name=='settings'}" class="md-3-line" ng-click="main.goToPage('settings', true)">
                <span class="eico eico-cog"></span>
                <div class="md-list-item-text">
                    <h3>Настройки ROOT</h3>
                </div>
            </md-list-item>

            <md-divider ></md-divider>
            <?php
            $link = url('/logout');
            ?>
            <md-list-item class="md-3-line" ng-click="main.goToLink('<?=$link?>')">
                <span class="eico eico-exit"></span>
                <div class="md-list-item-text">
                    <h3>Выход</h3>
                </div>
            </md-list-item>
            <md-list-item class="md-3-line">
            </md-list-item>
        </md-list>
    </md-content>
</md-sidenav>