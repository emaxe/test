<!doctype html >
<html lang="ru" ng-app="app" ng-controller="mainCtrl as main">
<head>
    <meta charset="UTF-8">
    <meta name="_app_host" content="{!! env('ADMIN_HOST', false) !!}"/>
    <meta name="contact" content="{{ $meta->email }}">
    <meta name="phone" content="{{ $meta->phone }}">
    <link rel="icon" type="image/png" href="{{ $meta->favicon }}">
    @if($meta->noIndex)
        <meta name="robots" content="noindex"/>
    @endif
    <title>{{ $meta->siteName }}</title>

    <link rel="stylesheet" href="{{ asset("css/stop.css") }}">
</head>
<body>

<img src="./tinymce/upload/site/close.gif" alt="close">
<div class="title">Сайт временно не работает</div>


{!! $meta->dopScripts or '' !!}
{!! $meta->metrika or '' !!}

</body>
</html>