<!doctype html >
<html lang="ru" ng-app="app" ng-controller="mainCtrl as main">
<head>
    <meta charset="UTF-8">
    <meta name="_app_host" content="{!! env('APP_HOST', false) !!}"/>
    <meta name="contact" content="{{ $meta->email }}">
    <meta name="phone" content="{{ $meta->phone }}">

    <meta name="yandex-verification" content="c732208c6f56ea7d" />

    @if(isset($meta->siteDescription) && strlen($meta->siteDescription)>0)
        <meta name="description" content="{{ $meta->siteDescription }}">
    @endif

    @if(isset($meta->siteKeywords) && strlen($meta->siteKeywords)>0)
        <meta name="Keywords" content="{{ $meta->siteKeywords }}">
    @endif

    <link rel="icon" type="image/png" href="{{ $meta->favicon }}">
    @if($meta->noIndex)
        <meta name="robots" content="noindex"/>
    @endif
    <title>{{ $meta->siteName }}</title>



    <link rel="stylesheet" href="{{ asset("css/theme.css") }}">
    <link rel="stylesheet" href="{{ asset("css/fancyBox.css") }}">
    <link rel="stylesheet" href="{{ asset("css/angular-theme.css") }}">
</head>
<body>


@foreach($blocks as $block)
    <section
            class="block block-{{ $block->value->name }}"
            id="block-{{ $block->value->name }}"
            {{ $block->value->attrs or '' }}
            {{ $block->vars['settings']->value->attrs or '--'}}
            >
        <div class="anchor" id="{{ $block->vars['settings']->value->anchor or '' }}"></div>
        @include('templates.'.$meta->theme.'.blocks.'.$block->value->name, ['block' => $block])
    </section>
@endforeach

<div class="up-btn"></div>

<script src="{{ asset('js/jQuery.js') }}"></script>
<script src="{{ asset('js/fancyBox.js') }}"></script>
<script src="{{ asset('js/angular-theme.js') }}"></script>
<script>
    new WOW().init();
</script>
<script src="{{ asset('js/bower-theme.js') }}"></script>
<script src="{{ asset('js/ngApp-theme.js') }}"></script>

<script>

    $(document).ready(function() {

        $(".f-img").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'autoDimensions'	: true,
            'speedIn'		:	600,
            'speedOut'		:	200,
            'overlayShow'	:	true
        });

        $("a[href*=\\#]").on("click", function(e){
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top
            }, {{ $meta->anchorScrollTime }});
            e.preventDefault();
        });

        var sF = function(){
            var scroll = $(this).scrollTop();
            if(scroll < 100){
                $('.up-btn').fadeOut();
            }
            else{
                $('.up-btn').fadeIn();
            }
        };

        $(window).scroll(function(e){
            sF();
        });

        sF();

        $.mask.definitions['~']='[+-]';

        $('.phone').mask('8(999) 999-9999', {placeholder: '8(xxx) xxx-xxxx'});

        $('.phoneext').mask("(999) 999-9999? x99999");

        $(".up-btn").on("click", function(e){
            $('html, body').stop().animate({
                scrollTop: 0
            }, {{ $meta->anchorScrollTime }});
            e.preventDefault();
        });

        return false;
    });

</script>

{!! $meta->dopScripts or '' !!}
{!! $meta->metrika or '' !!}

</body>
</html>