<?php

$v = $block->vars["data"]->value;

?>

<div class="bg" style="background-image: url({{ $v->bg }})"></div>
<div class="block-body">

    <div class="title">
        {!! $v->title !!}
    </div>

    <div class="wrap">
        <div class="os">
            <form action="#">
                <input class="os-item input" ng-model="main.os.name" type="text" name="name" placeholder="{{ $v->osInput1Title }}">
                <input class="os-item input" ng-model="main.os.phone" type="text" name="tel" placeholder="{{ $v->osInput2Title }}">
                <input class="os-item input" ng-model="main.os.email" type="text" name="email" placeholder="{{ $v->osInput3Title }}">
                <button class="os-item btn" ng-click="main.os.send($event)" href="#" class="btn_submit sendmail">{{ $v->osBtnTitle }}</button>
                <div class="space"></div>
            </form>
        </div>
    </div>

    <div class="text">{!! $v->text !!}</div>

</div>