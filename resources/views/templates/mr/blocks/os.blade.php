<?php
    $v = $block->vars["data"]->value;
?>

<div class="modal" ng-class="{show:main.os.is_ShowModal}">
    <div class="ov" ng-click="main.os.hideModal()"></div>


    <div class="os">
        <div class="title">{!! $v->osTitle !!}</div>
        <div class="subtitle">{!! $v->osSubtitle !!}</div>
        <form action="#">
            <input <?= (strlen($v->osInput1Class)>0)?'class="'.$v->osInput1Class.'"':'' ?> ng-model="main.os.name" type="{{ $v->osInput1Type }}" name="i1OSInput" placeholder="{{ $v->osInput1Title }}">
            <input <?= (strlen($v->osInput2Class)>0)?'class="'.$v->osInput2Class.'"':'' ?> ng-model="main.os.email" type="{{ $v->osInput2Type }}" name="i2OSInput" placeholder="{{ $v->osInput2Title }}">
            <input <?= (strlen($v->osInput3Class)>0)?'class="'.$v->osInput3Class.'"':'' ?> ng-model="main.os.phone" type="{{ $v->osInput3Type }}" name="i3OSInput" placeholder="{{ $v->osInput3Title }}">
            <button ng-click="main.os.send($event)" href="#" class="btn_submit sendmail">{{ $v->osBtnTitle }}</button>
        </form>
    </div>
</div>

<div class="block-body">

</div>