<?php
    $v = $block->vars["data"]->value;
    $items = $block->vars['data']->groupItems;

    $allItems = $block->vars['data']->groupItemsAll;
?>

<div class="bg" style="background-image: url({{ $v->bg }})"></div>
<div class="block-body">

    <div class="title">{{ $v->title }}</div>

    <div class="wrap">
        <div class="items1">
            <?php
            $i = 0;
            ?>
            @foreach($allItems['block-whyUs-items'] as $item)
                <?php
                $value = $item->value;
                $i++;
                ?>
                <div class="item">
                    <div class="number">{{ '0'.$i }}</div>
                    <div class="item-title">{!! $value->title !!}</div>
                    <div class="item-text">{!! $value->text !!}</div>
                </div>
            @endforeach
                <div class="space"></div>
        </div>

    </div>

    <div class="text">{{ $v->text }}</div>

    <div class="wrap">
        <div class="items2">
            <?php
            $i = 0;
            ?>
            @foreach($allItems['block-whyUs-items2'] as $item)
                <?php
                $value = $item->value;
                $i++;
                ?>
                <div class="item">
                    <div class="number">{{ $i }}</div>
                    <div class="item-title">{!! $value->text !!}</div>
                    <div class="item-img" style="background-image: url({{ $value->img }})" alt="{{ $v->text }}"></div>
                </div>
            @endforeach
        </div>
        <div class="space"></div>
    </div>


    <div class="btn" ng-click="main.os.showModal()">{{ $v->btnTitle }}</div>

</div>