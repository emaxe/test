<?php
    $v = $block->vars["data"]->value;
?>
<div class="block-body">

    <div class="logo"
         style="
                 width: <?= $meta->logoWidth ?>px;
                 height: <?= $meta->logoHeight ?>px;
                 background-image: url(<?= $v->logo ?>)
                 ">
        <a href="/"></a>

        <div class="slogan">
            {!! $v->slogan !!}
        </div>
    </div>

    <div class="text">
        {!! $v->text !!}

        <div class="img" style="background-image: url({{ $v->img }})"></div>
    </div>

    <div class="btn" ng-click="main.os.showModal()">
        {{ $v->btnTitle }}
    </div>

</div>