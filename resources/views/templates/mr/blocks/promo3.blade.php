<?php

$v = $block->vars["data"]->value;
$items = $block->vars['data']->groupItems;

?>

<div class="bg" style="background-image: url({{ $v->bg }})"></div>
<div class="block-body">

    <div class="title">
        {{ $v->title }}
    </div>

    <div class="items">
        @foreach($items as $item)
            <?php
            $value = ($item->value);
            ?>
            <div class="item">
                <div class="item-img" style="background-image: url({{ $value->img }})"></div>
                <div class="item-title">{{ $value->title }}</div>
                <div class="item-text">{!! $value->text !!}</div>
            </div>
        @endforeach
            <div class="space"></div>
    </div>
    <div class="space"></div>

    <div class="btn" ng-click="main.os.showModal()">{{ $v->btnTitle }}</div>

</div>