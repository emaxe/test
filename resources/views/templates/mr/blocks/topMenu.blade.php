<?php
$v = $block->vars["items"]->value;
$items = $block->vars['items']->groupItems;
?>

<div class="block-body">

    <div class="logo">
        <div class="logo-text"><span class="i1">{!! $v->logoText1 !!}</span><span class="i2">{!! $v->logoText2 !!}</span><a href="/"></a></div>
    </div>

    <div class="menu">
        @foreach($items as $item)
            <?php
                $value = $item->value;
            ?>
            <div class="menu-item">
                <a href="{{ $item->value->link }}">{{ $item->value->title }}</a>
            </div>
        @endforeach
    </div>


    <div class="phone">
        {!! $v->phone !!}
    </div>
    <div class="slogan">
        {!! $v->slogan !!}
    </div>
    <div class="at-sign">!</div>

</div>