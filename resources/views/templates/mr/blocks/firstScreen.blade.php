<?php

$v = $block->vars["data"]->value;
$items = $block->vars['data']->groupItems;

?>

<div class="bg" style="background-image: url({{ $v->bg }})"></div>

<div class="block-body">


    @if($v->showLine)
        <div class="line" style="left:{{ $v->lineLeft }}; top:{{ $v->lineTop }}"></div>
    @endif

    <div class="text">{!! $v->text !!}</div>

        <div class="btn" ng-click="main.os.showModal()">{{ $v->btnTitle }}</div>

        <div class="menu">
            @foreach($items as $item)
                <?php
                $value = $item->value;
                ?>
                <div class="menu-item">
                    <a href="{{ $item->value->link }}">{!! $item->value->text !!}</a>
                </div>
            @endforeach
        </div>

</div>