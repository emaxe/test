<?php
    $v = $block->vars["data"]->value;
    $items = $block->vars['data']->groupItems;
?>

<div class="bg" style="background-image: url({{ $v->bg }})"></div>
<div id="work-popup">
    <div class="wp-wrap">
        <div class="wp-title">-</div>
        <div class="wp-items">
            <div class="wp-items-wrap"></div>
        </div>
        <div class="wp-controlls">
            <div class="wp-btn wp-left"></div>
            <div class="wp-btn wp-right"></div>
            <div class="x">X</div>
        </div>
        <div class="wp-text"></div>
    </div>
</div>

<div class="block-body">

    <div class="title">
        <span>{{ $v->title }}</span>
    </div>

    <div ng-work class="items">
        <?php $i = 0 ?>
        @foreach($items as $item)
            <?php
            $i++;
            $value = ($item->value);
            ?>
            <div index="{{ $i }}"  gallery-id="{{ $value->gallery }}" gallery-title="{{ $value->title }}" class="item i{{ $i }}">
                <div class="img" style="background-image: url({{ $value->img }})"></div>
                {{--<div class="title">{{ $value->title }}</div>--}}
            </div>

        @endforeach

        <div class="space"></div>
    </div>

</div>