<?php

$v = $block->vars["data"]->value;

?>

<div class="block-body">

    <div class="box"></div>

    <div class="block-title">
        {!! $v->blockTitle !!}
    </div>

    <div class="title">
        {!! $v->title !!}
    </div>

    <div class="text">
        {!! $v->text !!}
    </div>

    <div class="btn" ng-click="main.os.showModal()">{{ $v->btnTitle }} <div class="arrow" style="left={{ $v->arrowLeft }}"></div>   </div>

</div>