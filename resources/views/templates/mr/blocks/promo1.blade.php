<?php
$v = $block->vars["data"]->value;
$items = $block->vars['data']->groupItems;
$i = 0;
?>

<div class="bg" style="background-image: url({{ $v->bg }})"></div>
<div class="block-body">

    <div class="title">
        {!! $v->title !!}
    </div>

    <div class="items">
        @foreach($items as $item)
            <?php
            $value = $item->value;
            $i++;
            ?>
            <div i="{{ $i }}" class="item i{{ $i }} {{ ($i==1)?'active':'' }}" ng-click="main.selectMaterial($event, this)">
                {{ $value->title }}
            </div>

            <div i="{{ $i }}" id="itemContent{{ $i }}" class="item-content i{{ $i }} {{ ($i==1)?'active':'' }}">
                <span>{!! $value->text !!}</span>
            </div>
        @endforeach
    </div>

    <div class="text">{!! $v->text !!}</div>

</div>