<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gallery_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('gallery_id')->unsigned()->default(0);
			$table->foreign('gallery_id')
				->references('id')
				->on('galleries')
				->onDelete('cascade');

			$table->string('title')->nullable();
			$table->text('description')->nullable();
			$table->string('img')->nullable();

			$table->integer('order')->default(100);
			$table->boolean('published')->nullable()->dafault(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gallery_items');
	}

}
