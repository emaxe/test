<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contents', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('pid')->unsigned()->default(0);
			$table->foreign('pid')
				->references('id')
				->on('contents')
				->onDelete('cascade');

			$table->integer('type_id')->unsigned()->default(0);
			$table->foreign('type_id')
				->references('id')
				->on('content_types')
				->onDelete('cascade');

			$table->string('name')->nullable();
			$table->string('title')->nullable();
			$table->string('group')->nullable();
			$table->string('parent_group')->nullable();

			$table->integer('order')->default(100);
			$table->boolean('published')->nullable()->dafault(false);

			$table->json('value')->nullable();
			$table->json('css')->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contents');
	}

}
