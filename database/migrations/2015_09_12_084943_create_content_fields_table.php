<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentFieldsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('content_fields', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('content_type_id')->unsigned()->default(0);
			$table->foreign('content_type_id')
				->references('id')
				->on('content_types')
				->onDelete('cascade');

			$table->integer('group_type_id')->unsigned()->default(0);

			$table->integer('select_id')->unsigned()->default(0);
			$table->foreign('select_id')
				->references('id')
				->on('dictionaries')
				->onDelete('cascade');

			$table->string('type');
			$table->string('name');
			$table->boolean('editable')->default(false);
			$table->string('default')->nullable();
			$table->string('title')->nullable();
			$table->string('description')->nullable();
			$table->integer('order')->default(100);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('content_fields');
	}

}
