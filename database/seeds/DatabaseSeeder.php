<?php

use App\Content;
use App\ContentField;
use App\ContentType;
use App\Gallery;
use App\GalleryItem;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(JsonLoader::class);
    }


}


class JsonLoader extends Seeder
{
    public function run()
    {

        $tables = [
            'users',
            'contents',
            'content_fields',
            'content_types',
            'dictionaries',
        ];

        foreach($tables as $table){
            DB::table($table)->delete();
        }

        $this->command->info('=============================================');
        $this->command->info('> Tables dropped');

        $disk = Storage::disk('seed');

        $loadJsonMass = function($model) use ($disk){
            $this->command->info('> JSON load start mass ['.$model.']..');
            $rootArr = [];
            $contents = json_decode($disk->get($model));
            if(is_array($contents)){
                foreach($contents as $content){
                    $arr = [];
                    foreach((array)$content as $key=>$value){
                        if(is_array($value) || is_object($value)){
                            if(is_object($value) && isset($value->_SMART) && $value->_SMART){
                                switch ($value->_TYPE){
                                    case 'bcrypt':
                                        $value = bcrypt($value->_VALUE);
                                        break;
                                    default:
                                        $value = $value->_VALUE;
                                        break;
                                }
                            }else{
                                $value = "".json_encode($value);
                            }
                        }
                        $arr[$key] = $value;
                    }
                    $rootArr[] = $arr;
                    $this->command->info('--> Added content.');
                }
                $this->command->info('> OK! File ['.$model.'] is readed');
            }else{
                $this->command->info('> WARNING! File ['.$model.'] is empty..');
            }
            return $rootArr;
        };

        $loadJson = function($model, $f) use ($disk){
            $this->command->info('> JSON load start ['.$model.']..');
//            $rootArr = [];
//            if($disk->exists($model)){
//                $this->command->info('> OK! File ['.$model.'] is exist..');
            $contents = json_decode($disk->get($model));
//				$this->command->info(json_encode($contents));
            if(is_array($contents)){
                foreach($contents as $content){
                    $arr = [];
                    foreach((array)$content as $key=>$value){
                        if(is_array($value) || is_object($value)){
                            if(is_object($value) && isset($value->_SMART) && $value->_SMART){
                                switch ($value->_TYPE){
                                    case 'bcrypt':
                                        $value = bcrypt($value->_VALUE);
                                        break;
                                    default:
                                        $value = $value->_VALUE;
                                        break;
                                }
                            }else{
                                $value = json_encode($value);
                            }
                        }
                        $arr[$key] = $value;
                    }
                    $f($arr);
//                        $rootArr[] = $arr;
                    $this->command->info('--> Added content.');
                }
                $this->command->info('> OK! File ['.$model.'] is readed');
//                    return $rootArr;
            }else{
                $this->command->info('> WARNING! File ['.$model.'] is empty..');
            }
//            }else{
//                $this->command->info('> WARNING! File ['.$model.'] is not exist..');
//            }
        };

        $readModelMass = function($model) use($disk, $loadJsonMass){
            $this->command->info('');
            $this->command->info('> Read the model mass ['.$model.']');
            $this->command->info('---------------------------------------------');

            $rootArr = [];



            if(substr($model,0,1)!='_'){
                $files = $disk->files($model);
                foreach($files as $file){
                    $this->command->info('> Read the file mass ['.$file.']');
                    $this->command->info('.............................................');

                    $arr = $loadJsonMass($file);
                    foreach($arr as $item){
                        $rootArr[] = $item;
                    }
                }

                $this->command->info('> Model ['.$model.'] prepare... '.count($rootArr));


                $insertMass = function($rArr, $first)use($model){
                    $this->command->info('> Model ['.$model.'] inserting mass '.count($rArr));
                    $rootArr = $rArr;
                    if($first) array_pull($rootArr, 0);

                    if(ctype_upper(substr($model,0,1))){
                        $reflection = new ReflectionClass('\App\\'.$model);
                        $class = $reflection->newInstance();
                        $class->insert($rootArr);
                        $this->command->info('> Model ['.$model.'] is created! '.count($rootArr));
                    }else{
                        DB::table($model)->insert($rootArr);
                        $this->command->info('> Table ['.$model.'] is filled! '.count($rootArr));
                    }
                };

                $example = $rootArr[0];
                $rArr = [];
                $first = true;
                foreach($rootArr as $arr){
//                    $this->command->info('**********************************');
                    $item = $example;
                    foreach($arr as $key => $value){
                        $item[$key] = $value;
                    }

                    $arr = $item;

//                    foreach($arr as $key => $value){
//                        $this->command->info('-> '.$key.' => '.$value);
//                    }

                    $rArr[] = $arr;
                    if(count($rArr)>70){
                        $insertMass($rArr, $first);
                        $rArr = [];
                        $first = false;
                    }
                }

                if(count($rArr)>0) $insertMass($rArr, $first);

            }else{
                $this->command->info('> Skip ['.$model.'] ... '.count($rootArr));
            }


//            DB::table($model)->insert($rootArr);




        };

        $readModel = function($model) use($disk, $loadJson){
            $this->command->info('');
            $this->command->info('> Read the model ['.$model.']');
            $this->command->info('---------------------------------------------');
            $files = $disk->files($model);
            foreach($files as $file){
                $this->command->info('> Read the file ['.$file.']');
                $this->command->info('.............................................');

                $reflection = new ReflectionClass('\App\\'.$model);
                $class = $reflection->newInstance();
                $loadJson($file, function($arr) use($class){
                    $class->create($arr);
                });

            }
        };


        $directories = $disk->directories();
        foreach($directories as $directory){
            if(substr($directory,0,1)!='_'){
                if($disk->exists($directory.'/.json')){
                    $readModelMass($directory);
                }else{
                    $readModel($directory);
                }
            }
        }

        $this->command->info('=============================================');
    }


}