var elixir = require('laravel-elixir');
var gulp = require('gulp');
var stripDebug = require('gulp-strip-debug');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.extend('strip', function(script) {
    new elixir.Task('strip', function() {
        return gulp.src(script)
            .pipe(stripDebug())
            .pipe(gulp.dest(function(file) {
                return file.base;
            }));
    });
});


elixir(function(mix) {
 mix
     .less('app.less')
     .less('theme.less')
     .less('stop.less')

     .scripts([
      'ng/admin/homeCtrl.js',
     ],'public/js/ng/admin/homeCtrl.js')
     //.strip('public/js/ng/admin/homeCtrl.js')
     .scripts([
      'ng/admin/metaCtrl.js',
     ],'public/js/ng/admin/metaCtrl.js')
     .scripts([
      'ng/admin/smsCtrl.js',
     ],'public/js/ng/admin/smsCtrl.js')
     .scripts([
      'ng/admin/menuCtrl.js',
     ],'public/js/ng/admin/menuCtrl.js')
     .scripts([
      'ng/admin/blockCtrl.js',
     ],'public/js/ng/admin/blockCtrl.js')
     .scripts([
      'ng/admin/galleriesCtrl.js',
     ],'public/js/ng/admin/galleriesCtrl.js')
     .scripts([
         'ng/admin/blocksCtrl.js',
     ],'public/js/ng/admin/blocksCtrl.js')
     .scripts([
         'ng/admin/settingsCtrl.js',
     ],'public/js/ng/admin/settingsCtrl.js')


     .scripts([
      'ng/admin/dialogs/dialogCreateContentCtrl.js',
     ],'public/js/ng/admin/dialogs/dialogCreateContentCtrl.js')
     .scripts([
      'ng/admin/dialogs/dialogCreateGalleryCtrl.js',
     ],'public/js/ng/admin/dialogs/dialogCreateGalleryCtrl.js')
     .scripts([
         'ng/admin/dialogs/dialogCreateFieldCtrl.js',
     ],'public/js/ng/admin/dialogs/dialogCreateFieldCtrl.js')
     .scripts([
         'ng/admin/dialogs/dialogCreateTypeCtrl.js',
     ],'public/js/ng/admin/dialogs/dialogCreateTypeCtrl.js')
     .scripts([
         'ng/admin/dialogs/dialogEditTypeCtrl.js',
     ],'public/js/ng/admin/dialogs/dialogEditTypeCtrl.js')
     .scripts([
         'ng/admin/dialogs/dialogEditContentCtrl.js',
     ],'public/js/ng/admin/dialogs/dialogEditContentCtrl.js')
     .scripts([
         'ng/admin/dialogs/dialogEditFieldCtrl.js',
     ],'public/js/ng/admin/dialogs/dialogEditFieldCtrl.js')

     .styles([
      'fancybox/dist/css/jquery.fancybox.css',
     ],'public/css/bowerLibs.css','node_modules')
     .styles([
      'fancybox/dist/css/jquery.fancybox.css',
     ],'public/css/fancyBox.css','node_modules')
     .styles([
      'angular-material/angular-material.min.css',
      'angular-material-data-table/dist/md-data-table.min.css',
     ],'public/css/ngMaterial.css','node_modules')


     .scripts([
      'angular-ui-tinymce/src/tinymce.js',
     ],'public/js/angular-ui-tinymce.js','node_modules')
     .scripts([
      'ng/admin/ngApp.js'
     ],'public/js/ngApp.js')
     .scripts([
      'jquery/dist/jquery.min.js',
      'bootstrap/dist/js/bootstrap.min.js',
      'fancybox/dist/js/jquery.fancybox.pack.js',
     ],'public/js/bowerLibs.js','node_modules')
     .scripts([
      'fancybox/dist/js/jquery.fancybox.pack.js',
     ],'public/js/fancyBox.js','node_modules')
     .scripts([
      'jquery/dist/jquery.min.js',
      'jquery.maskedinput/src/jquery.maskedinput.js',
     ],'public/js/jQuery.js','node_modules')

     .scripts([
      'angular/angular.js',
      //'angular-modal-service/dst/angular-modal-service.min.js',
      'oclazyload/dist/ocLazyLoad.js',
      'angular-resource/angular-resource.min.js',
      'angular-animate/angular-animate.min.js',
      'angular-aria/angular-aria.min.js',
      'angular-material/angular-material.min.js',
      'angular-material-data-table/dist/md-data-table.min.js',
      'angular-upload-file/angular-file-upload.min.js',
      //'vue/vue.min.js',
      //'vue-resource/vue-resource.min.js',
     ],'public/js/angular.js','node_modules')

     .scripts([
      'angular/angular.min.js',
      'wowjs/dist/wow.min.js',
      'angular-resource/angular-resource.min.js',
      //'ng-toast/dist/ngToast.min.js',
      'angular-animate/angular-animate.min.js',
      'ng-file-upload/dist/ng-file-upload-all.min.js',
      //'angular-input-masks/releases/angular-input-masks-standalone.min.js',
     ],'public/js/angular-theme.js','node_modules')
     .scripts([
      'angular-toastr/dist/angular-toastr.tpls.min.js',
      'ng-mask/dist/ngMask.min.js',
     ],'public/js/bower-theme.js','node_modules')
     .scripts([
      'ng/theme/ngApp.js',
      'ng/theme/utils/config.js',
      'ng/theme/mainCtrl.js',
      'ng/theme/utils/directives.js',
      'ng/theme/utils/models.js',
     ],'public/js/ngApp-theme.js')
     //.strip('public/js/ngApp-theme.js')

     .styles([
      'angular-toastr/dist/angular-toastr.min.css',
     ],'public/css/angular-theme.css','node_modules')

     .scripts([
      'ng/admin/ngApp.js',
      'ng/admin/utils/RecursionHelper.js',
      'ng/admin/mainCtrl.js',
      'ng/admin/leftCtrl.js',
      'ng/admin/dialogs/dialogCtrl.js',
      'ng/admin/utils/blocksList.js',
      'ng/admin/utils/config.js',
      'ng/admin/utils/models.js',
      'ng/admin/utils/storage.js',
      'ng/admin/utils/directives.js',
      'ng/admin/utils/helper.js',
      'ng/admin/directives/*.*',
     ],'public/js/ngApp.js')

  .copy('node_modules/fancybox/dist/img/fancybox_loading@2x.gif', 'public/img/fancybox_loading@2x.gif')
  .copy('node_modules/fancybox/dist/img/fancybox_loading.gif', 'public/img/fancybox_loading.gif')
  .copy('node_modules/fancybox/dist/img/blank.gif', 'public/img/blank.gif')
  .copy('node_modules/fancybox/dist/img/fancybox_overlay.png', 'public/img/fancybox_overlay.png')
  .copy('node_modules/fancybox/dist/img/fancybox_sprite.png', 'public/img/fancybox_sprite.png')
  .copy('node_modules/fancybox/dist/img/fancybox_sprite@2x.png', 'public/img/fancybox_sprite@2x.png')
 ;
});



